<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/laravel', function () {
    return view('welcome');
});

Route::get('holaMundo', 'unidad2\HolaMundoController@hola');

/*Ruta practica 1*/
Route::get('practica1', 'unidad2\practica1\TablaSencillaController@tabla');

Route::get('ver','unidad2\UsuariosBD\UsuariosController@ver_consulta');

Route::get('insertar','unidad2\UsuariosBD\UsuariosController@insert');

/*Tarea 3*/
Route::get('cuentas','unidad2\tarea3\CuentaController@vista');
Route::get('cuentas/insertar','unidad2\tarea3\CuentaController@formulario');
Route::get('cuentas/eliminar','unidad2\tarea3\CuentaController@eliminar');
Route::get('cuentas/actualizar','unidad2\tarea3\CuentaController@actualizar');

Route::post('insertar_form','unidad2\tarea3\CuentaController@insert');
Route::get('delete_form','unidad2\tarea3\CuentaController@delete');

Route::post('actualiza_form','unidad2\tarea3\CuentaController@ver_datos');
Route::put('update_form','unidad2\tarea3\CuentaController@update');

Route::post('cambiarContraseña','unidad2\tarea3\CuentaController@cambiarContraseña');

Route::post('vistaReestablecerPass','unidad2\tarea3\CuentaController@changePassView');

Route::put('changePass_form','unidad2\tarea3\CuentaController@changePass');

