<?php
namespace App\Models\unidad3\tarea1;
use Illuminate\Database\Eloquent\Model;

class EstudianteModel extends Model
{
protected $table = 'estudiante';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['nombre','sexo','materia','calificacion'];
}