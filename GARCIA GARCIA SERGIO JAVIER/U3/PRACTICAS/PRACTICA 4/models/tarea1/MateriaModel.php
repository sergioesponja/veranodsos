<?php
namespace App\Models\unidad3\tarea1;
use Illuminate\Database\Eloquent\Model;

class MateriaModel extends Model
{
protected $table = 'materia';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $filltable = ['nombre'];
}