<?php
namespace App\Http\Controllers\unidad3\tarea1;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Models\unidad3\tarea1\EstudianteModel;
use App\Models\unidad3\tarea1\MateriaModel;

class EstudianteController extends Controller
{


 public function claveMateria($nombre)
 {
 	$listado = MateriaModel::select('id AS clave','nombre AS nombre')->where('nombre',$nombre)->get()->first();
 	return $listado;
 }




 public function insertEstudiante(Request $request)
 {
   $nombre       = $request->input('nombre');
   $sexo         = $request->input('sexo');
   $idmateria    = $request->input('claveM');
   $calificacion = $request->input('calificacion');

   EstudianteModel::create(
   [
     'nombre' => $nombre,
     'sexo' => $sexo,
     'materia' => $idmateria,
     'calificacion' => $calificacion
   ]);

   return redirect()->to('nuevoEstudiante');

 }

 public function formularioEstudiante()
 {
  $enviar = MateriaModel::pluck('nombre','nombre');
  return view('unidad3/tarea1/NuevoEstudiante')->with('listaM',$enviar);
 }



}