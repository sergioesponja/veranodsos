<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Perros</title>

        <!-- Fonts -->
        


       
    </head>

    <script src="js/jquery.min.js"></script>
    <script src="js/unidad3/tarea1/scriptMateria.js"></script>
    
    <body>
        <center>

            {!!Form::open(array('url'=>'insert-estudiante','method'=>'POST','autocomplete'=>'off'))!!}

            <table border=1>

                 <tr>

                    <td>{!!Form::label('Nombre:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

                    <td>{!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Ingrese nombre del estudiante'])!!} </td>

                </tr>

                <tr>

                    <td>{!!Form::label('Sexo:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

                    <td>{!!Form::text('sexo',null,['id'=>'sexo','class'=>'form-control','placeholder'=>'M ó F'])!!} </td>

                </tr>

                <tr>
                    <td>{!!Form::label('Materia:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

                    <td>{{Form::select('idmateria',$listaM,null, ['id' => 'id_de_selector', 'placeholder' => 'SELECCIONE UNA OPCION'] ) }} </td>
                </tr>

               <tr>

                    <td>{!!Form::label('Clave materia:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

                    <td>{!!Form::text('claveM',null,['id'=>'claveM','class'=>'form-control','placeholder'=>'clave','readonly'])!!} </td>

                </tr>

                <tr>

                    <td>{!!Form::label('Calificacion:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

                    <td>{!!Form::text('calificacion',null,['id'=>'calificacion','class'=>'form-control','placeholder'=>'Calificacion obtenida'])!!}</td>

                </tr>

                

             </table>

                 <br/>
                      {!!Form::submit('Registrar',null,['id'=>'registrar','content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
                 <br/>


             {!!Form::close()!!}

         </center>
    
    </body>




</html>