<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/laravel', function () {
    return view('welcome');
});

Route::get('holaMundo', 'unidad2\HolaMundoController@hola');

/*Ruta practica 1*/
Route::get('practica1', 'unidad2\practica1\TablaSencillaController@tabla');

Route::get('ver','unidad2\UsuariosBD\UsuariosController@ver_consulta');

Route::get('insertar','unidad2\UsuariosBD\UsuariosController@insert');

/*Tarea 3*/
Route::get('cuentas','unidad2\tarea3\CuentaController@vista');

Route::get('cuentas/insertar','unidad2\tarea3\CuentaController@formulario');

//Route::get('cuentas/eliminar','unidad2\tarea3\CuentaController@eliminar');
//Route::get('cuentas/actualizar','unidad2\tarea3\CuentaController@actualizar');
//Route::get('delete_form','unidad2\tarea3\CuentaController@delete');
//Route::post('actualiza_form','unidad2\tarea3\CuentaController@ver_datos');

Route::post('insertar_form','unidad2\tarea3\CuentaController@insert');

Route::put('update_form','unidad2\tarea3\CuentaController@update');

Route::post('cambiarContraseña','unidad2\tarea3\CuentaController@cambiarContraseña');

Route::post('vistaReestablecerPass','unidad2\tarea3\CuentaController@changePassView');

Route::put('changePass_form','unidad2\tarea3\CuentaController@changePass');

Route::get('editar/{id}','unidad2\tarea3\CuentaController@editar');

Route::get('editarContraseña/{id}','unidad2\tarea3\CuentaController@editarContraseña');

Route::get('borrar/{id}/{x}','unidad2\tarea3\CuentaController@confirmarView');

Route::get('baja/{id}/{x}','unidad2\tarea3\CuentaController@confirmarView');

Route::post('confirmar_form','unidad2\tarea3\CuentaController@confirmarBorrar');




//PRACTICA 3: 12 DE JULIO DE 2019 --CRUD BIENES--
Route::get('bienes/todos','unidad2\practica3\BienesController@verDatos');

Route::get('bienes/en-existencia','unidad2\practica3\BienesController@vistaEnExistencia');

Route::get('bienes/sin-existencia','unidad2\practica3\BienesController@vistaInexistentes');

Route::get('bienes/insertar','unidad2\practica3\BienesController@formularioNuevoBien');

Route::post('insert-bien','unidad2\practica3\BienesController@insert');

Route::get('bienes/editar/{id}','unidad2\practica3\BienesController@editar');

Route::put('bienes/update_form','unidad2\practica3\BienesController@update');

Route::get('bienes/baja/{id}','unidad2\practica3\BienesController@baja');

Route::get('bienes/alta/{id}','unidad2\practica3\BienesController@alta');

Route::get('bienes/borrar/{id}','unidad2\practica3\BienesController@confirmarView');

Route::post('bienes/confirmar_form','unidad2\practica3\BienesController@confirmarBorrar');


//UNIDAD 3: 13 DE JULIO DE 2019 --USO DE AJAX (PRIMERA PRACTICA)--
 Route::get('estudiantes/asincrono','unidad3\Ajax\AjaxController@ver_selector');
 Route::get('estudiantes/lista/{id}','unidad3\Ajax\AjaxController@listado_estudiantes');




 //UNIDAD 2: 15 DE JULIO DE 2019 --EXAMEN 1OP--
 //---------------------------------------------------------------------------------------------------------CUENTAS
 Route::get('examen1OP/cuentas','unidad2\examen1OP\CuentaController@vista');

Route::get('examen1OP/cuentas/insertar','unidad2\examen1OP\CuentaController@formulario');

Route::post('examen1OP/cuentas/insertar_form','unidad2\examen1OP\CuentaController@insert');

Route::put('examen1OP/cuentas/update_form','unidad2\examen1OP\CuentaController@update');

Route::post('examen1OP/cuentas/cambiarContraseña','unidad2\examen1OP\CuentaController@cambiarContraseña');

Route::post('examen1OP/cuentas/vistaReestablecerPass','unidad2\examen1OP\CuentaController@changePassView');

Route::put('examen1OP/cuentas/changePass_form','unidad2\examen1OP\CuentaController@changePass');

Route::get('examen1OP/cuentas/editar/{id}','unidad2\examen1OP\CuentaController@editar');

Route::get('examen1OP/cuentas/editarContraseña/{id}','unidad2\examen1OP\CuentaController@editarContraseña');

Route::get('examen1OP/cuentas/borrar/{id}/{x}','unidad2\examen1OP\CuentaController@confirmarView');

Route::get('examen1OP/cuentas/baja/{id}/{x}','unidad2\examen1OP\CuentaController@baja');

Route::get('examen1OP/cuentas/alta/{id}/{x}','unidad2\examen1OP\CuentaController@alta');

Route::post('examen1OP/cuentas/confirmar_form','unidad2\examen1OP\CuentaController@confirmarBorrar');

//---------------------------------------------------------------------------------------------------------JUEGOS
Route::get('juegos/todos','unidad2\examen1OP\JuegoController@verDatos');

Route::get('juegos/en-existencia','unidad2\examen1OP\JuegoController@vistaEnExistencia');

Route::get('juegos/sin-existencia','unidad2\examen1OP\JuegoController@vistaInexistentes');

Route::get('juegos/insertar','unidad2\examen1OP\JuegoController@formularioNuevoBien');

Route::post('insert-juego','unidad2\examen1OP\JuegoController@insert');

Route::get('juegos/editar/{id}','unidad2\examen1OP\JuegoController@editar');

Route::put('juegos/update_form','unidad2\examen1OP\JuegoController@update');

Route::get('juegos/baja/{id}','unidad2\examen1OP\JuegoController@baja');

Route::get('juegos/alta/{id}','unidad2\examen1OP\JuegoController@alta');

Route::get('juegos/borrar/{id}','unidad2\examen1OP\JuegoController@confirmarView');

Route::post('juegos/confirmar_form','unidad2\examen1OP\JuegoController@confirmarBorrar');



//UNIDAD 3: 16 DE JULIO DE 2019 --PERROS--
Route::get('perritos','unidad3\Ajax\AjaxController@seleccion_perro');
Route::get('lista_perros/{id}','unidad3\Ajax\AjaxController@perro_listado');


 //TAREA1: 16 DE JULIO DE 2019 --ESTUDIANTE CON SELECTOR DINAMICO CON AJAX--
 Route::get('nuevoEstudiante','unidad3\tarea1\EstudianteController@formularioEstudiante');
 Route::get('claveMateria/{id}','unidad3\tarea1\EstudianteController@claveMateria');
 Route::post('insert-estudiante','unidad3\tarea1\EstudianteController@insertEstudiante');