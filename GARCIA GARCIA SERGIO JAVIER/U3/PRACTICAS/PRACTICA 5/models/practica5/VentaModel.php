<?php
namespace App\Models\unidad3\practica5;
use Illuminate\Database\Eloquent\Model;

class VentaModel extends Model
{
protected $table = 'venta';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['software','precio','licencia','id_cliente'];
}