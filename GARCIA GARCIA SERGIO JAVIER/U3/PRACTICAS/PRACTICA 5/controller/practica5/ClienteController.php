<?php
namespace App\Http\Controllers\unidad3\practica5;
use App\Http\Controllers\Controller;
use App\Models\unidad3\practica5\ClienteModel;
use App\Models\unidad3\practica5\VentaModel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class ClienteController extends Controller
{


public function insert_cliente(Request $request)
{
$nombre = $request->nombre_var;
$apellido = $request->apellido_var;
$direccion = $request->direccion_var;
$rfc = $request->rfc_var;

ClienteModel::create(['nombre' => $nombre, 'apellido' => $apellido, 'direccion' => $direccion, 'rfc' => $rfc]);
return response()->json(['respuesta' => ' Registrado correctamente']);
}


public function visualizar_por_cliente(Request $request)
{
 $id_cliente = $request->input('id_cliente_var');
 $consulta_ver = VentaModel::select('software','precio','licencia')->where('id_cliente', $id_cliente)->get();
 return $consulta_ver;
}

public function eliminar_cliente(Request $request){


$id = $request->id_var;
$registro = VentaModel::find()->where('id_cliente',$id_var);
$registro->delete();
$registro = ClienteModel::find()->where('id',$id_var);
$registro->delete();
 return response()->json(['respuesta' => ' Eliminado correctamente']);
}


public function eliminar_venta(Request $request){


$id = $request->id_venta_var;
$registro = VentaModel::find($id);
$registro->delete();
 return response()->json(['respuesta' => ' Eliminado correctamente']);
}


public function actualizar_venta(Request $request)
{
 $id_venta = $request->id_venta_var;
 $registro = VentaModel::find($id_venta);
 $registro->software = $request->software_var;
 $registro->precio = $request->precio_var;
 $registro->licencia = $request->licencia_var;

 $registro->save();
 return response()->json(['respuesta' => 'Actualizado correctamente']);
}
}