<?php
namespace App\Http\Controllers\unidad3\practicaGrupal;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Models\unidad3\practicaGrupal\OrdenModel;
use App\Models\unidad3\practicaGrupal\ComidaModel;

class RestaurantController extends Controller
{


 public function claveComida($nombre)
 {
 	$listado = ComidaModel::select('id AS clave','nombre AS nombre','costo AS costo','tipo AS tipo')->where('nombre',$nombre)->get()->first();
 	return $listado;
 }




 public function insertOrden(Request $request)
 {
   $comida       = $request->input('claveC');
   $cantidad       = $request->input('cantidad');
   $costoT         = $request->input('costoT');

   OrdenModel::create(
   [
    'comida' => $comida,
     'cantidad' => $cantidad,
     'costoT' => $costoT
   ]);

   return redirect()->to('nuevaOrden');

 }

 public function formularioComida()
 {
  $enviar = ComidaModel::pluck('nombre','nombre');
  return view('unidad3/practicaGrupal/NuevaOrden')->with('listaC',$enviar);
 }




}