<?php
namespace App\Models\unidad3\practicaGrupal;
use Illuminate\Database\Eloquent\Model;

class ComidaModel extends Model
{
protected $table = 'comida';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['nombre','costo','tipo'];
}