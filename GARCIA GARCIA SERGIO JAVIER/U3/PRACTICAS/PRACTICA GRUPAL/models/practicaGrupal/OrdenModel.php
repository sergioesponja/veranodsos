<?php
namespace App\Models\unidad3\practicaGrupal;
use Illuminate\Database\Eloquent\Model;

class OrdenModel extends Model
{
protected $table = 'orden';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['comida','cantidad','costoT'];
}