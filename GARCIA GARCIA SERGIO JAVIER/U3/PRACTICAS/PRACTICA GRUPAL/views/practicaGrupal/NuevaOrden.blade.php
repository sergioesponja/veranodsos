<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Orden de comida</title>

        <!-- Fonts -->
        


       
    </head>

    <script src="js/jquery.min.js"></script>
    <script src="js/unidad3/practicaGrupal/scriptComida.js"></script>
    
    <body>
        <center>

            {!!Form::open(array('url'=>'insert-orden','method'=>'POST','autocomplete'=>'off'))!!}

            <table border=1>




                <tr>
                    <td>{!!Form::label('Comida:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

                    <td>{{Form::select('idcomida',$listaC,null, ['id' => 'id_de_selector', 'placeholder' => 'SELECCIONE UNA OPCION'] ) }} </td>
                </tr>

               <tr>

                    <td>{!!Form::hidden('claveC',null,['id'=>'claveC','class'=>'form-control'])!!} </td>

               </tr>

               <tr>

                    <td>{!!Form::label('Tipo de comida:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

                    <td>{!!Form::text('tipo',null,['id'=>'tipo','class'=>'form-control','placeholder'=>'tipo de comida','readonly'])!!}</td>

                </tr>

                <tr>

                    <td>{!!Form::label('Costo individual:$',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

                    <td>{!!Form::text('costo',null,['id'=>'costo','class'=>'form-control','placeholder'=>'ingrese la cantidad'])!!}</td>

                </tr>



                <tr>

                    <td>{!!Form::label('Cantidad:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

                    <td>{!!Form::text('cantidad',null,['id'=>'cantidad','class'=>'form-control','placeholder'=>'ingrese la cantidad'])!!}</td>

                </tr>

                <tr>

                    <td>{!!Form::label('Costo total $:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

                    <td>{!!Form::text('costoT',null,['id'=>'costoT','class'=>'form-control','placeholder'=>'costo total','readonly'])!!}</td>
                    <td>{!!Form::label('.00',null,['length'=>'10'])!!} <span style="color:red">*</span></td>


                </tr>


             </table>

                 <br/>
                      {!!Form::submit('Registrar',null,['id'=>'registrar','content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
                 <br/>


             {!!Form::close()!!}

         </center>
    
    </body>




</html>