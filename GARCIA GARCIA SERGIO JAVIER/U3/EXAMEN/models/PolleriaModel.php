<?php
namespace App\Models\unidad3\examen1op;
use Illuminate\Database\Eloquent\Model;

class PolleriaModel extends Model
{
protected $table = 'polleria';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['dueño','rfc_empresa','direccion','id_ventas'];
}