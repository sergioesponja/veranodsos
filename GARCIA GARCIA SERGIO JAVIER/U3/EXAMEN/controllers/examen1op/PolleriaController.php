<?php
namespace App\Http\Controllers\unidad3\examen1op;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Models\unidad3\examen1op\PolleriaModel;
use App\Models\unidad3\examen1op\VentasModel;


class PolleriaController extends Controller
{


public function ver_Api_Post(Request $request)
{
  $venta = $request->id_ventas;
  $consulta_ver = PolleriaModel::select('dueño','rfc_empresa','direccion','fecha_creacion','id_ventas')->where('id_ventas',$venta)->get();
  return $consulta_ver;
}


public function insertar_Api_Post(Request $variable)
{
 $dueño = $variable->dueño;
 $rfc_empresa = $variable->rfc_empresa;
 $direccion = $variable->direccion;
 $id_ventas = $variable->id_ventas;

PolleriaModel::create(['dueño' => $dueño, 'rfc_empresa' => $rfc_empresa,'direccion' => $direccion, 'id_ventas' => $id_ventas]);
return response()->json(['respuesta' => ' Registrado correctamente']);

}

public function actualizar_Api_Put(Request $variable)
{
 $id = $variable->id;
 $registro = PolleriaModel::find($id);

 $registro->dueño = $variable->dueño;
 $registro->rfc_empresa = $variable->rfc_empresa;
 $registro->direccion = $variable->direccion;
 $registro->id_ventas = $variable->id_ventas;

 $registro->save();

return response()->json(['respuesta' => ' Actualizado correctamente']);

}

public function eliminar_api_Get(Request $request)
{
$id = $request->id;
 $registro = PolleriaModel::find($id);

 $registro->delete();
 return response()->json(['respuesta' => ' Eliminado correctamente']);
}

}

?>