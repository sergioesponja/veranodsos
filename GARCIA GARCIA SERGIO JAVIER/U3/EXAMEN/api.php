<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//23 DE JULIO DE 2019 --PRACTICA UNO CON API--
Route::get('ver_Api','ApiController\ApiController@ver_Api');
Route::post('ver_Api_Post','ApiController\ApiController@ver_Api_Post');
Route::post('insertar_Api_Post','ApiController\ApiController@insertar_Api_Post');

//TAREA:23 DE JULIO DE 2019 --EDITAR Y ELIMINAR REGISTROS--
Route::put('actualizar_Api_Put','ApiController\ApiController@actualizar_Api_Put');
Route::put('eliminar_Api_Get','ApiController\ApiController@eliminar_Api_Get');

//PRACTICA 5: 24 DE JULIO DE 2019 --CRUD VENTAS DE SOFTWARE--
Route::post('insertar_cliente','unidad3\practica5\ClienteController@insert_cliente');
Route::get('visualizar_cliente','unidad3\practica5\ClienteController@visualizar_por_cliente');
Route::get('eliminar_cliente','unidad3\practica5\ClienteController@eliminar_cliente');
Route::put('actualizar_venta','unidad3\practica5\ClienteController@actualizar_venta');
Route::get('eliminar_venta','unidad3\practica5\ClienteController@eliminar_venta');

//PRACTICA 6: 26 DE JULIO DE 2019 --CONSUMO DE API--
Route::get('api_perro','unidad3\practica6\ConsumoController@consumo_busqueda'); 
Route::post('api_perro_registrar','unidad3\practica6\ConsumoController@api_Registrar');
Route::put('api_perro_actualizar','unidad3\practica6\ConsumoController@api_Actualizar');
Route::put('api_perro_eliminar','unidad3\practica6\ConsumoController@api_Eliminar');

//EXAMEN U3 1OP: 27 DE JULIO DE 2019 --API Y CONSUMO DE API--    
Route::post('ver_polleria_Post','unidad3\examen1op\PolleriaController@ver_Api_Post');
Route::post('insertar_polleria_Post','unidad3\examen1op\PolleriaController@insertar_Api_Post');
Route::put('actualizar_polleria_Put','unidad3\examen1op\PolleriaController@actualizar_Api_Put');
Route::put('eliminar_polleria_Put','unidad3\examen1op\PolleriaController@eliminar_Api_Get');


