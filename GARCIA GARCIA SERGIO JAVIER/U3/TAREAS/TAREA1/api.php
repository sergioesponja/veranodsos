<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//23 DE JULIO DE 2019 --PRACTICA UNO CON API--
Route::get('ver_Api','ApiController\ApiController@ver_Api');
Route::post('ver_Api_Post','ApiController\ApiController@ver_Api_Post');
Route::post('insertar_Api_Post','ApiController\ApiController@insertar_Api_Post');

//TAREA:23 DE JULIO DE 2019 --EDITAR Y ELIMINAR REGISTROS--
Route::put('actualizar_Api_Put','ApiController\ApiController@actualizar_Api_Put');
Route::get('eliminar_Api_Get','ApiController\ApiController@eliminar_Api_Get');
