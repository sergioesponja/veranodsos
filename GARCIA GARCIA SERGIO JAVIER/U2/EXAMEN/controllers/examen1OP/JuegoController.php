<?php
namespace App\Http\Controllers\unidad2\examen1OP;
use App\Http\Controllers\Controller;
use App\Models\unidad2\examen1OP\JuegoModel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class JuegoController extends Controller
{
 public function verDatos()//ver todos los Juegos
 {
  $consulta = JuegoModel::all();
   return view('unidad2\examen1OP\juegos\ALLJuegosView')->with('datos',$consulta);
 }


 public function vistaEnExistencia()//Juegos en existencia
 {
   $tabla = JuegoModel::all()->where('estatus',1);

      return view('unidad2\examen1OP\juegos\JuegosEnExistenciaView')->with('datos',$tabla);
 }


 public function vistaInexistentes()//Juegos sin existencia
 {
   $tabla = JuegoModel::all()->where('estatus',0);

      return view('unidad2\examen1OP\juegos\JuegosInexistentesView')->with('datos',$tabla);
 }



 public function formularioNuevoBien()
 {
  return view('unidad2\examen1OP\juegos\InsertJuegoView');
 }

 public function insert(Request $request)
 {
  $nombre      = $request->input('nombre');
  $tipo = $request->input('tipo');


  JuegoModel::create(['nombre'      => $nombre, 
                       'tipo' => $tipo]);
  
  return redirect()->to('juegos/todos');
 }



 public function update(Request $request)
 {
  $id         = $request->input('id');
  $actualizar = JuegoModel::find($id);

  $actualizar->nombre         = $request->nombre;
  $actualizar->tipo    = $request->tipo;
  $actualizar->save();

  return redirect()->to('juegos/todos');
 }

public function editar($id)
{
 $actualizar = JuegoModel::find($id);
 $consulta   = JuegoModel::where('id',$id)->take(1)->first();

 return view('unidad2\examen1OP\juegos\ActualizarDatosView')->with('modificar',$consulta);

}


public function borrar($id)
{
 $actualizar = JuegoModel::find($id);
 $actualizar->delete();

 return redirect()->to('juegos/todos');
}


public function cancelar()
{
  return redirect()->to('juegos/todos');
}
 

public function alta($id)
 {
   $actualizar = JuegoModel::find($id);
   $actualizar->estatus = 1;
   $actualizar->save();

   return redirect()->to('juegos/todos');
 }



 public function baja($id)
 {
   $actualizar = JuegoModel::find($id);
   $actualizar->estatus = 0;
   $actualizar->save();

   return redirect()->to('juegos/todos');
 }


 public function confirmarBorrar(Request $request)
 {
  $id = $request->input('id');

 $actualizar = JuegoModel::find($id);
 $actualizar->delete();

return redirect()->to('juegos/todos');
}


public function confirmarView($id){
return view('unidad2\examen1OP\juegos\VistaConfirmacion')->with('id',$id);
}



}