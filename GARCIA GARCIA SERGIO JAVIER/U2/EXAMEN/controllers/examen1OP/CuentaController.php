<?php
namespace App\Http\Controllers\unidad2\examen1OP;
use App\Http\Controllers\Controller;
use App\Models\unidad2\examen1OP\CuentaModel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class CuentaController extends Controller
{
 public function verDatos()
 {
 	$consulta = CuentaModel::all();
   return $consulta;
 }

 public function vista()
 {
 	 $tabla = CuentaModel::all();

      return view('unidad2\examen1OP\cuentas\CuentasView')->with('datos',$tabla);
 }

 public function formulario()
 {
 	return view('unidad2\examen1OP\cuentas\InsertCuentaView');
 }

 public function insert(Request $request)
 {
  $nombre     = $request->input('nombre');
  $usuario    = $request->input('usuario');
  $password = $request->input('password');

  CuentaModel::create(['nombre' => $nombre,  
  	                   'usuario' => $usuario, 
  	                   'password' => $password]);
  
  return redirect()->to('examen1OP\cuentas');
 }



 public function update(Request $request)
 {
  $idcuenta = $request->input('idcuenta');
  $actualizar = CuentaModel::find($idcuenta);

  $actualizar->nombre     = $request->nombre;
  $actualizar->usuario    = $request->usuario;
  $actualizar->save();

  return redirect()->to('examen1OP\cuentas');
 }




public function editar($idcuenta)
{
 $actualizar = CuentaModel::find($idcuenta);
 $consulta = CuentaModel::where('id',$idcuenta)->take(1)->first();

 return view('unidad2\examen1OP\cuentas\ActualizarDatos')->with('modificar',$consulta);

}


public function borrar($idcuenta)
{
 $actualizar = CuentaModel::find($idcuenta);
 $actualizar->delete();

 return redirect()->to('examen1OP\cuentas');
}

public function cancelar()
{
  return redirect()->to('examen1OP\cuentas');
}
 

public function editarContraseña($idcuenta)
{
 $actualizar = CuentaModel::find($idcuenta);

 return view('unidad2\examen1OP\cuentas\ChangePassword')->with('modificar',$actualizar);

}

public function changePass(Request $request)
 {
 $idcuenta = $request->input('idcuenta');
 $actualizar = CuentaModel::find($idcuenta);

 $actualizar->password = $request->password;
 $actualizar->save();

  return redirect()->to('examen1OP\cuentas');
 }

 public function baja($idcuenta)
 {
   $actualizar = CuentaModel::find($idcuenta);
   $actualizar->activo = 0;
   $actualizar->save();

   return redirect()->to('examen1OP\cuentas');
 }

 public function alta($idcuenta)
 {
   $actualizar = CuentaModel::find($idcuenta);
   $actualizar->activo = 1;
   $actualizar->save();

   return redirect()->to('examen1OP\cuentas');
 }



 public function confirmarBorrar(Request $request)
 {
  $idcuenta = $request->input('idcuenta');
 $actualizar = CuentaModel::find($idcuenta);
 $actualizar->delete();

return redirect()->to('examen1OP\cuentas');
}


public function confirmarView($idcuenta,$x){
return view('unidad2\examen1OP\cuentas\VistaConfirmarDelete')->with('idcuenta',$idcuenta);
}



}