<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cambiar contraseña</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

       
    </head>
    
    <body>
        <center>

        {!!Form::open(array('url'=>'examen1OP/cuentas/changePass_form','method'=>'PUT','autocomplete'=>'off'))!!}
         
         <table border='1'>

            {!!Form::hidden('idcuenta',$modificar->id,['id'=>'idcuenta'])!!}


          <tr>

         <td>{!!Form::label('Contraseña:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::password('password',null,['id'=>'password','class'=>'form-control','placeholder'=>'Ingrese contraseña'])!!}
         </td>
         
         </tr>

         <tr>

         <td>{!!Form::label('Confirma tu contraseña:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::password('confirmPass',null,['id'=>'confirmPass','class'=>'form-control','placeholder'=>'Ingrese contraseña'])!!}
         </td>
         
         </tr>


            
        </table>

        <br/>
        {!!Form::submit('Actualizar',null,['id'=>'registrar','content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
        <br/>
        <span style="color:red">*</span> {!!Form::label('Campos obligatorios',null,['length'=>'10'])!!}

        {!!Form::close()!!}


         {!!Form::open(array('url'=>'examen1OP/cuentas','method'=>'GET','autocomplete'=>'off'))!!}

        {!!Form::submit('Cancelar',null,['id'=>'cancelar','content'=>'<span>Cancelar</span>','class'=>'btn btn-primary'])!!}


        {!!Form::close()!!}

        </center>
    
    </body>
</html>