<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cuentas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

       
    </head>
    
    <body>
        <center>

       <form>
        
        <table border="1">
    
        <thead>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Usuario</th>
                  <th>Fecha</th>
                  <th>STATUS(1=activo|0=inactivo)</th>
     </thead>


      @foreach ($datos as $c)

      <tr>
        <td>{{ $c->id }}</td>
        <td>{{ $c->nombre }}</td>
        <td>{{ $c->usuario }}</td>
        <td>{{ $c->f_registro }}</td>
        <td>{{ $c->activo }}</td>
        <td>
         <a href="cuentas/editar/{{$c->id }}"> Editar</a> |
         <a href="cuentas/editarContraseña/{{$c->id }}"> Nueva contraseña</a> |         
         <a href="cuentas/borrar/{{$c->id }}/{{0}}"> Borrar</a> |
         <a href="cuentas/baja/{{$c->id }}/{{0}}"> Baja</a>
         <a href="cuentas/alta/{{$c->id }}/{{0}}"> Alta</a>
        </td>
      </tr>
      @endforeach

        </table>
      

       </form>

       {!!Form::open(array('url'=>'examen1OP/cuentas/insertar','method'=>'GET','autocomplete'=>'off'))!!}

       {!!Form::submit('Insertar registro',null,['id'=>'insertar','content'=>'<span>Insertar registro</span>','class'=>'btn btn-primary'])!!}

       {!!Form::close()!!}


        </center>
    
    </body>
</html>