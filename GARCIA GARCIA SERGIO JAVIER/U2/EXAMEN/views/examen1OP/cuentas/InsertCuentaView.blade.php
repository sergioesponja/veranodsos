<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cuentas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

       
    </head>
    
    <body>
        <center>

        {!!Form::open(array('url'=>'examen1OP/cuentas/insertar_form','method'=>'POST','autocomplete'=>'off'))!!}
         
         <table border='1'>

            <tr>

         <td>{!!Form::label('Nombre:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Ingrese nombre(s) del usuario'])!!}
         </td>

     </tr>


     <tr>

         <td>{!!Form::label('Usuario:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('usuario',null,['id'=>'usuario','class'=>'form-control','placeholder'=>'Ingrese nombre de Usuario'])!!}
         </td>
         
     </tr>

     <tr>

         <td>{!!Form::label('Contraseña:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::password('password',null,['id'=>'password','class'=>'form-control','placeholder'=>'Ingrese contraseña'])!!}
         </td>
         
     </tr>

            
        </table>

        <br/>
        {!!Form::submit('Registrar',null,['id'=>'registrar','content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
        <br/>

        {!!Form::close()!!}


        

        {!!Form::open(array('url'=>'examen1OP/cuentas','method'=>'GET','autocomplete'=>'off'))!!}

        {!!Form::submit('Cancelar',null,['id'=>'cancelar','content'=>'<span>Cancelar</span>','class'=>'btn btn-primary'])!!}


        {!!Form::close()!!}

        <br/>

        <span style="color:red">*</span> {!!Form::label('Campos obligatorios',null,['length'=>'10'])!!}

        </center>
    
    </body>
</html>