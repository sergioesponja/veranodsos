<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cuentas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

       
    </head>
    
    <body>
        <center>




       {!!Form::open(array('url'=>'juegos/todos','method'=>'GET','autocomplete'=>'off'))!!}

           {!!Form::submit('Ver todos',null,['id'=>'todos','content'=>'<span>Ver todos</span>','class'=>'btn btn-primary'])!!}

       {!!Form::close()!!}
       


       {!!Form::open(array('url'=>'juegos/sin-existencia','method'=>'GET','autocomplete'=>'off'))!!}

           {!!Form::submit('Ver inexistentes',null,['id'=>'inexistentes','content'=>'<span>Ver inexistentes</span>','class'=>'btn btn-primary'])!!}

       {!!Form::close()!!}






       <form>
        
        <table border="1">
    
        <thead>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Tipo</th>
                  <th>En existencia</th>
     </thead>


      @foreach ($datos as $c)

      <tr>
        <td>{{ $c->id }}</td>
        <td>{{ $c->nombre }}</td>
        <td>{{ $c->tipo }}</td>
        <td>{{ $c->estatus }}</td>

        <td>
         <a href="editar/{{$c->id }}"> Editar Campos</a> |  
         <a href="baja/{{$c->id }}"> Baja</a> |         
         <a href="borrar/{{$c->id }}"> Borrar</a>
        </td>
      </tr>

      @endforeach

        </table>
      

       </form>

       

        </center>
    
    </body>
</html>