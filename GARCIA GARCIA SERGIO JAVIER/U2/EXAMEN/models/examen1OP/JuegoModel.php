<?php
namespace App\Models\unidad2\examen1OP;
use Illuminate\Database\Eloquent\Model;

class JuegoModel extends Model
{
protected $table = 'juego';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['nombre','tipo'];


}