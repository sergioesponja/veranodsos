<?php
namespace App\Models\unidad2\examen1OP;
use Illuminate\Database\Eloquent\Model;

class CuentaModel extends Model
{
protected $table = 'cuenta';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['nombre','usuario','password'];


}