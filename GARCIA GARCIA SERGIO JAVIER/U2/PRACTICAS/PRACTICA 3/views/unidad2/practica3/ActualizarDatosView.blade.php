<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cuentas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

       
    </head>
    
    <body>
        <center>

        {!!Form::open(array('url'=>'bienes/update_form','method'=>'PUT','autocomplete'=>'off'))!!}
         
         <table border='1'>

            {!!Form::hidden('id',$modificar->id,['id'=>'id'])!!}

            <tr>

         <td>{!!Form::label('Nombre:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('nombre',$modificar->nombre,['id'=>'nombre','class'=>'form-control','placeholder'=>'Ingrese nombre(s) del bien'])!!}
         </td>

     </tr>

      <tr>

         <td>{!!Form::label('Descripcion:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('descripcion',$modificar->descripcion,['id'=>'descripcion','class'=>'form-control','placeholder'=>'Ingrese descripcion'])!!}
         </td>
         
     </tr>


     <tr>

         <td>{!!Form::label('Fecha registro:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('f_registro',$modificar->f_registro,['id'=>'f_registro','class'=>'form-control','placeholder'=>'YYYY-MM-DD'])!!}
         </td>
         
     </tr>
           
        </table>

        <br/>
        <span style="color:red">*</span> {!!Form::label('Campos obligatorios',null,['length'=>'10'])!!}
        <br/>
        <br/>


        {!!Form::submit('Actualizar',null,['id'=>'registrar','content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
        <br/>
        

        {!!Form::close()!!}
        <br/>
        <br/>



        {!!Form::open(array('url'=>'bienes/todos','method'=>'GET','autocomplete'=>'off'))!!}

        {!!Form::submit('Cancelar',null,['id'=>'cancelar','content'=>'<span>Cancelar</span>','class'=>'btn btn-primary'])!!}


        {!!Form::close()!!}





        </center>
    
    </body>
</html>