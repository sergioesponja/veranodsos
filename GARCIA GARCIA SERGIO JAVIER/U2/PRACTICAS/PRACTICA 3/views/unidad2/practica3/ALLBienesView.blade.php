<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cuentas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

       
    </head>
    
    <body>
        <center>


<table>
         <th>
       {!!Form::open(array('url'=>'bienes/insertar','method'=>'GET','autocomplete'=>'off'))!!}

           {!!Form::submit('Insertar bien',null,['id'=>'insertar','content'=>'<span>Insertar bien</span>','class'=>'btn btn-primary'])!!}

       {!!Form::close()!!}
       </th>

       <th>
       {!!Form::open(array('url'=>'bienes/en-existencia','method'=>'GET','autocomplete'=>'off'))!!}

           {!!Form::submit('Ver existentes',null,['id'=>'existentes','content'=>'<span>Ver existentes</span>','class'=>'btn btn-primary'])!!}

       {!!Form::close()!!}
      </th>

       <th>
       {!!Form::open(array('url'=>'bienes/sin-existencia','method'=>'GET','autocomplete'=>'off'))!!}

           {!!Form::submit('Ver inexistentes',null,['id'=>'inexistentes','content'=>'<span>Ver inexistentes</span>','class'=>'btn btn-primary'])!!}

       {!!Form::close()!!}
      </th>

    </table>





       <form>
        
        <table border="1">
    
        <thead>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Descripcion</th>
                  <th>F. registro</th>
                  <th>En existencia</th>
     </thead>


      @foreach ($datos as $c)

      <tr>
        <td>{{ $c->id }}</td>
        <td>{{ $c->nombre }}</td>
        <td>{{ $c->descripcion }}</td>
        <td>{{ $c->f_registro }}</td>
        <td>{{ $c->existencia }}</td>

        <td>
         <a href="editar/{{$c->id }}"> Editar Campos</a> | 
         <a href="alta/{{$c->id }}"> Alta</a> | 
         <a href="baja/{{$c->id }}"> Baja</a> |         
         <a href="borrar/{{$c->id }}"> Borrar</a>
        </td>
      </tr>

      @endforeach

        </table>
      

       </form>

       

        </center>
    
    </body>
</html>