<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cuentas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

       
    </head>
    
    <body>
        <center>

        {!!Form::open(array('url'=>'insert-bien','method'=>'POST','autocomplete'=>'off'))!!}
         
         <table border='1'>

            <tr>

         <td>{!!Form::label('Nombre:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Ingrese nombre del bien'])!!}
         </td>

     </tr>

      <tr>

         <td>{!!Form::label('Descripcion:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('descripcion',null,['id'=>'descripcion','class'=>'form-control','placeholder'=>'Ingrese descripcion'])!!}
         </td>
         
     </tr>

     
            
        </table>

        <br/>
        {!!Form::submit('Registrar',null,['id'=>'registrar','content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
        <br/>

        {!!Form::close()!!}


        

        {!!Form::open(array('url'=>'bienes/todos','method'=>'GET','autocomplete'=>'off'))!!}

        {!!Form::submit('Cancelar',null,['id'=>'cancelar','content'=>'<span>Cancelar</span>','class'=>'btn btn-primary'])!!}


        {!!Form::close()!!}

        <br/>

        <span style="color:red">*</span> {!!Form::label('Campos obligatorios',null,['length'=>'10'])!!}

        </center>
    
    </body>
</html>