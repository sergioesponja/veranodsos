<?php
namespace App\Http\Controllers\unidad2\practica3;
use App\Http\Controllers\Controller;
use App\Models\unidad2\practica3\BienesModel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class BienesController extends Controller
{
 public function verDatos()//ver todos los bienes
 {
 	$consulta = BienesModel::all();
   return view('unidad2\practica3\ALLBienesView')->with('datos',$consulta);
 }


 public function vistaEnExistencia()//Bienes en existencia
 {
 	 $tabla = BienesModel::all()->where('existencia',1);

      return view('unidad2\practica3\BienesEnExistenciaView')->with('datos',$tabla);
 }


 public function vistaInexistentes()//Bienes sin existencia
 {
   $tabla = BienesModel::all()->where('existencia',0);

      return view('unidad2\practica3\BienesInexistentesView')->with('datos',$tabla);
 }



 public function formularioNuevoBien()
 {
 	return view('unidad2\practica3\InsertBienView');
 }

 public function insert(Request $request)
 {
  $nombre      = $request->input('nombre');
  $descripcion = $request->input('descripcion');


  BienesModel::create(['nombre'      => $nombre, 
  	                   'descripcion' => $descripcion]);
  
  return redirect()->to('bienes/todos');
 }



 public function update(Request $request)
 {
  $id         = $request->input('id');
  $actualizar = BienesModel::find($id);

  $actualizar->nombre         = $request->nombre;
  $actualizar->descripcion    = $request->descripcion;
  $actualizar->f_registro    = $request->f_registro;
  $actualizar->save();

  return redirect()->to('bienes/todos');
 }

public function editar($id)
{
 $actualizar = BienesModel::find($id);
 $consulta   = BienesModel::where('id',$id)->take(1)->first();

 return view('unidad2\practica3\ActualizarDatosView')->with('modificar',$consulta);

}


public function borrar($id)
{
 $actualizar = BienesModel::find($id);
 $actualizar->delete();

 return redirect()->to('bienes/todos');
}


public function cancelar()
{
  return redirect()->to('bienes/todos');
}
 

public function alta($id)
 {
   $actualizar = BienesModel::find($id);
   $actualizar->existencia = 1;
   $actualizar->save();

   return redirect()->to('bienes/todos');
 }



 public function baja($id)
 {
   $actualizar = BienesModel::find($id);
   $actualizar->existencia = 0;
   $actualizar->save();

   return redirect()->to('bienes/todos');
 }


 public function confirmarBorrar(Request $request)
 {
  $id = $request->input('id');

 $actualizar = BienesModel::find($id);
 $actualizar->delete();

return redirect()->to('bienes/todos');
}


public function confirmarView($id){
return view('unidad2\practica3\VistaConfirmacion')->with('id',$id);
}



}