<?php
namespace App\Models\unidad2\practica3;
use Illuminate\Database\Eloquent\Model;

class BienesModel extends Model
{
protected $table = 'bienes';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['nombre','descripcion','f_registro'];


}