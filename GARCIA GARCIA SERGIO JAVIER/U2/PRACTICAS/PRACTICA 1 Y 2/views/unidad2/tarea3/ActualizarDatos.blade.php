<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cuentas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

       
    </head>
    
    <body>
        <center>

        {!!Form::open(array('url'=>'update_form','method'=>'PUT','autocomplete'=>'off'))!!}
         
         <table border='1'>

            {!!Form::hidden('idcuenta',$modificar->id,['id'=>'idcuenta'])!!}

            <tr>

         <td>{!!Form::label('Nombre:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('nombre',$modificar->nombre,['id'=>'nombre','class'=>'form-control','placeholder'=>'Ingrese nombre(s) del usuario'])!!}
         </td>

     </tr>

      <tr>

         <td>{!!Form::label('Ap. Paterno:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('ap_paterno',$modificar->ap_paterno,['id'=>'ap_paterno','class'=>'form-control','placeholder'=>'Ingrese apellido paterno'])!!}
         </td>
         
     </tr>

     <tr>

         <td>{!!Form::label('Ap. Materno:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('ap_materno',$modificar->ap_materno,['id'=>'ap_materno','class'=>'form-control','placeholder'=>'Ingrese apellido materno'])!!}
         </td>
         
     </tr>

     <tr>

         <td>{!!Form::label('Usuario:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('usuario',$modificar->usuario,['id'=>'usuario','class'=>'form-control','placeholder'=>'Ingrese nombre de Usuario'])!!}
         </td>
         
     </tr>


            
        </table>

        <br/>
        <span style="color:red">*</span> {!!Form::label('Campos obligatorios',null,['length'=>'10'])!!}
        <br/>
        <br/>


        {!!Form::submit('Actualizar',null,['id'=>'registrar','content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
        <br/>
        

        {!!Form::close()!!}
        <br/>
        <br/>



        {!!Form::open(array('url'=>'cuentas','method'=>'GET','autocomplete'=>'off'))!!}

        {!!Form::submit('Cancelar',null,['id'=>'cancelar','content'=>'<span>Cancelar</span>','class'=>'btn btn-primary'])!!}


        {!!Form::close()!!}





        </center>
    
    </body>
</html>