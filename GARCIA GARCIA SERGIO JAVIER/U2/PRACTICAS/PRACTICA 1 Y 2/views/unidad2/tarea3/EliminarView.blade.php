<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cuentas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

       
    </head>
    
    <body>
        <center>

        {!!Form::open(array('url'=>'delete_form','method'=>'GET','autocomplete'=>'off'))!!}
         
         <table border='1'>

            <tr>

         <td>{!!Form::label('Id:',null,['length'=>'10'])!!} <span style="color:red">*</span></td>

         <td>
         {!!Form::text('idcuenta',null,['id'=>'idcuenta','class'=>'form-control','placeholder'=>'Ingrese ID del usuario'])!!}
         </td>

     </tr>
           
        </table>

        <br/>
        {!!Form::submit('Buscar',null,['id'=>'buscar','content'=>'<span>Buscar</span>','class'=>'btn btn-primary'])!!}
        <br/>
        <span style="color:red">*</span> {!!Form::label('Campos obligatorios',null,['length'=>'10'])!!}

        {!!Form::close()!!}

        </center>
    
    </body>
</html>