<?php
namespace App\Http\Controllers\unidad2\tarea3;
use App\Http\Controllers\Controller;
use App\Models\unidad2\tarea3\CuentaModel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class CuentaController extends Controller
{
 public function verDatos()
 {
 	$consulta = CuentaModel::all();
   return $consulta;
 }

 public function vista()
 {
 	 $tabla = CuentaModel::all()->where('activo',1);

      return view('unidad2\tarea3\CuentasView')->with('datos',$tabla);
 }

 public function formulario()
 {
 	return view('unidad2\tarea3\InsertCuentaView');
 }

 public function insert(Request $request)
 {
  $nombre     = $request->input('nombre');
  $ap_paterno = $request->input('ap_paterno');
  $ap_materno = $request->input('ap_materno');
  $usuario    = $request->input('usuario');
  $password = $request->input('password');

  CuentaModel::create(['nombre' => $nombre, 
  	                   'ap_paterno' => $ap_paterno, 
  	                   'ap_materno' => $ap_materno, 
  	                   'usuario' => $usuario, 
  	                   'password' => $password]);
  
  return redirect()->to('cuentas');
 }

/*
 public function eliminar()
 {
  return view('unidad2\tarea3\EliminarView');
 }


 public function delete(Request $request)
 {
    $idcuenta = $request->input('idcuenta');
 	$eliminar = CuentaModel::find($idcuenta);
 	$eliminar->delete();

 	return 'Eliminado correctamente';
 }


 public function actualizar()
 {
   return view('unidad2\tarea3\BusquedaView');
 }


public function ver_datos(Request $request)
{
	$idcuenta = $request->input('idcuenta');
 $consulta = CuentaModel::where('id',$idcuenta)->take(1)->first();

 return view('unidad2\tarea3\ActualizarDatos')->with('modificar',$consulta);
}
*/

 public function update(Request $request)
 {
  $idcuenta = $request->input('idcuenta');
  $actualizar = CuentaModel::find($idcuenta);

  $actualizar->nombre     = $request->nombre;
  $actualizar->ap_paterno = $request->ap_paterno;
  $actualizar->ap_materno = $request->ap_materno;
  $actualizar->usuario    = $request->usuario;
  $actualizar->save();

  return redirect()->to('cuentas');
 }

/*
  public function cambiarContraseña()
 {
   return view('unidad2\tarea3\BusquedaPass');
 }

 public function changePassView(Request $request)
 {

   $idcuenta = $request->input('idcuenta');
   $consulta = CuentaModel::where('id',$idcuenta)->take(1)->first();

 	return view('unidad2\tarea3\ChangePassword')->with('modificar',$consulta);
 }

 
*/


public function editar($idcuenta)
{
 $actualizar = CuentaModel::find($idcuenta);
 $consulta = CuentaModel::where('id',$idcuenta)->take(1)->first();

 return view('unidad2\tarea3\ActualizarDatos')->with('modificar',$consulta);

}


public function borrar($idcuenta)
{
 $actualizar = CuentaModel::find($idcuenta);
 $actualizar->delete();

 return redirect()->to('cuentas');
}

public function cancelar()
{
  return redirect()->to('cuentas');
}
 

public function editarContraseña($idcuenta)
{
 $actualizar = CuentaModel::find($idcuenta);

 return view('unidad2\tarea3\ChangePassword')->with('modificar',$actualizar);

}

public function changePass(Request $request)
 {
 $idcuenta = $request->input('idcuenta');
 $actualizar = CuentaModel::find($idcuenta);

 $actualizar->password = $request->password;
 $actualizar->save();

  return redirect()->to('cuentas');
 }

 public function baja($idcuenta)
 {
   $actualizar = CuentaModel::find($idcuenta);
   $actualizar->activo = 0;
   $actualizar->save();

   return redirect()->to('cuentas');
 }


 public function confirmarBorrar(Request $request)
 {
  $idcuenta = $request->input('idcuenta');
  $x = $request->input('x');

if($x = 0)
{
 $actualizar = CuentaModel::find($idcuenta);
 $actualizar->delete();

}else 

if($x = 1)
{
 $actualizar = CuentaModel::find($idcuenta);
   $actualizar->activo = 0;
   $actualizar->save();
  
}
return redirect()->to('cuentas');
}




public function confirmarView($idcuenta,$x){
return view('unidad2\tarea3\VistaConfirmarDelete')->with('idcuenta',$idcuenta)->with('x',$x);
}



}