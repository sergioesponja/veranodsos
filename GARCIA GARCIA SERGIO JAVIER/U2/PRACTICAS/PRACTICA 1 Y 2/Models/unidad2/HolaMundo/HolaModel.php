<?php
namespace App\Models\unidad2\HolaMundo;
use Illuminate\Database\Eloquent\Model;

class HolaModel extends Model
{
protected $table = 'users';
protected $primarykey = 'rfc';
public $incrementing = false;
public $timestamps = false; 

protected $filltable = ['rfc','nombre'];


}