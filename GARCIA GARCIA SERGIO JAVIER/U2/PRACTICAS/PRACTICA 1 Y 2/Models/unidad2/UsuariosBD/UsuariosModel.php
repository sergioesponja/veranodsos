<?php
namespace App\Models\unidad2\UsuariosBD;
use Illuminate\Database\Eloquent\Model;

class UsuariosModel extends Model
{
protected $table = 'users';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['id','rfc'];


}