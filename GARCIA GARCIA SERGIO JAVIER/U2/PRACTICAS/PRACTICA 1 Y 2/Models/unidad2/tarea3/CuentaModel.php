<?php
namespace App\Models\unidad2\tarea3;
use Illuminate\Database\Eloquent\Model;

class CuentaModel extends Model
{
protected $table = 'cuenta';
protected $primarykey = 'idcuenta';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['nombre','ap_paterno','ap_materno','usuario','password'];


}