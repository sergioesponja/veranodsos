<?php
namespace App\Http\Controllers\unidad4\examen1op;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use GuzzleHttp\Client;
//use App\Models\unidad3\examen1op\Polleria;
//use App\Models\unidad3\examen1op\Ventas;


class ConsumoControladorPolleria extends Controller
{
  public function consumo_BusquedaPolleria()
    {
      //los datos de la polleria
  $respuesta = $this->peticion('POST',"http://dsos.ito/api/ver_polleria_Post",
  ['headers' => ['Content-Type' => 'application/x-www-form-urlencoded',
  'X-Requested-With' => 'XMLHttpRequest'],
  'form_params' => ['id_ventas' => 2]]);
  $retorno_datos = json_decode($respuesta);
  return response()->json($retorno_datos);
    }


    public function api_RegistraPolleria()
    {
        $respuesta = $this->peticion('POST',"http://dsos.ito/api/insertar_polleria_Post",[
          'headers' => [
             'Content-Type' => 'application/x-www-form-urlencoded',
             'X-Requested-With' => 'XMLHttpRequest'
          ],
          'form_params' => [
            'dueño' => 'Domingo',
            'rfc_empresa' => 'LIFGI9999',
            'direccion'  =>  'Ciudad de Oaxaca',
            'id_ventas' => 2
          ]
        ]);
        $datos = json_decode($respuesta);

        return response()->json($datos);
    }

    public function api_Actualiza()
    {
        $respuesta = $this->peticion('PUT',"http://dsos.ito/api/actualizar_polleria_Put",[
          'headers' => [
             'Content-Type' => 'application/x-www-form-urlencoded',
             'X-Requested-With' => 'XMLHttpRequest'
          ],
          'form_params' => [
            'id' => 3,
            'dueño' => 'Pedro',
            'direccion' => 'Las lomas de Culto',
            'rfc_empresa' => 'SSSSS10',
           'id_ventas' => 1
          ]
        ]);
        $datos = json_decode($respuesta);

        return response()->json($datos);
    }

    public function api_EliminarPolleria()
    {
        $respuesta = $this->peticion('PUT',"http://dsos.ito/api/eliminar_polleria_Put",[
          'headers' => [
             'Content-Type' => 'application/x-www-form-urlencoded',
             'X-Requested-With' => 'XMLHttpRequest'
          ],
          'form_params' => [
            'id' => 3
          ]
        ]);
        $datos = json_decode($respuesta);

        return response()->json($datos);
    }
 
 }

 ?>
