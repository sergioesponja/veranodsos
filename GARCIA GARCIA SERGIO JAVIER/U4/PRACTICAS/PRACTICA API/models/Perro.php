<?php
namespace App\Models\unidad3\Ajax;
use Illuminate\Database\Eloquent\Model;

class Perro extends Model
{
protected $table = 'perro';
protected $primarykey = 'id';
public $incrementing = true;
public $timestamps = false; 

protected $fillable = ['nombre','raza'];
}