<?php
namespace App\Models\practica4;
use Illuminate\Database\Eloquent\Model;

class modelo_materia extends Model
{
      protected $table ='materias';
      protected $primarykey='id';
      public $timestamps=false;
      protected $fillable =['id','materia','clave_materia'];

}
?>
