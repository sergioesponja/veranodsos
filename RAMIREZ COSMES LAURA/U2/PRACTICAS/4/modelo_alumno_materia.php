<?php
namespace App\Models\practica4;
use Illuminate\Database\Eloquent\Model;

class modelo_alumno_materia extends Model{
    protected $table = 'formulario';
    protected $primarykey = 'id';
    public $timestamp = false;
    protected $fillable = ['id', 'nombre_completo','materia','clave_materia','calificacion'];
}
 ?>
