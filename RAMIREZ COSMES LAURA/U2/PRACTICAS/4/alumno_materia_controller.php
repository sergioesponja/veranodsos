<?php
namespace App\Http\Controllers\practica4;
use App\Http\Controllers\Controller;
use App\Models\practica4\modelo_alumno_materia;
use App\Models\practica4\modelo_materia;
use Illuminate\Http\Request;
use Iluminate\Support\Facades\Input;

class alumno_materia_controller extends Controller
{
  public function materia_listado($materia){
    $consulta =modelo_materia::select('materia','clave_materia')
    ->where('materia', $materia)->get();
    return $consulta;

  }
  public function seleccion_materia(){
    $ver=modelo_materia::pluck('materia','materia');
    return view('practica4/practica4')
          ->with('lista_m',$ver);
  }

  public function insertar_form(Request $request){
    $nombre =$request->input('nombres');
    $mat=$request->input('idmateria');
    $clave=$request->input('idselector2');
    $calificacion=$request->input('calificacion');
    modelo_alumno_materia::create(['nombre_completo'=>$nombre,'materia'=>$mat,
    'clave_materia'=>$clave,'calificación'=>$calificacion]);
    return 'Registrado';
  }
}
?>
