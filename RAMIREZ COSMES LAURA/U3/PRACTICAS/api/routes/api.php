<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('ver_api','api\apiController@ver_api');
Route::post('ver_api_post','api\apiController@ver_api_post');
Route::post('insertar_post','api\apiController@insertar_api');
Route::post('eliminar_post','api\apiController@eliminar_api');
Route::post('actualizar_post','api\apiController@actualizar_api');

Route::post('insertar_cliente','api\neulogyController@insertar_api');
Route::post('ver_venta_cliente','api\neulogyController@ver_ventas_cliente');
Route::post('eliminar_ventas','api\neulogyController@eliminar_ventas');
Route::post('actualizar_venta','api\neulogyController@actualizar_venta');


Route::get('ver_Api','ApiController\ApiControlador@ver_Api');

Route::post('ver_Api_Post','ApiController\ApiControlador@ver_Api_Post');

Route::post('insertar_Api','ApiController\ApiControlador@insertar_Api');

Route::put('actualizar_Api','ApiController\ApiControlador@actualizar_Api');

Route::put('baja_Api','ApiController\ApiControlador@baja_Api');

Route::put('eliminar_Api','ApiController\ApiControlador@eliminar_Api');
