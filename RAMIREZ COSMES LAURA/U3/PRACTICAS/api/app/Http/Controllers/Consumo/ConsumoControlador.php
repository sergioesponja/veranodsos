<?php

namespace App\Http\Controllers\Consumo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Iluminate\Support\Facades\Input;
use GuzzleHttp\Client;
use App\Models\ajax\modelo_perro;

class ConsumoControlador extends Controller{
  public function consumo_Busqueda()
  {
    $respuesta = $this->peticion('GET',"http://dsos.ito:81/api/ver_Api",[
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
        'X-Requested-With' => 'XMLHttpRequest'
      ],
      'form_params' => [
        'raza' => 'vainillita'
      ]
    ]);
    $retorno_datos = json_decode($respuesta);
    return response()->json($retorno_datos);
  }

  public function api_Registra()
  {
      $respuesta = $this->peticion('POST',"http://dsos.ito:81/api/insertar_Api",[
        'headers' => [
           'Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'nombre' => 'Becerrito',
          'raza' => 'Chihuahua'
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
  }

  public function api_Actualiza()
  {
      $respuesta = $this->peticion('PUT',"http://dsos.ito:81/api/actualizar_Api",[
        'headers' => [
           'Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'nombre' => 'PATITO',
          'raza' => 'pitbull'
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
  }

  public function api_Baja()
  {
      $respuesta = $this->peticion('PUT',"http://dsos.ito:81/api/baja_Api",[
        'headers' => [
           'Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'raza' => 'Poodle'
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
  }

  public function api_Eliminar()
  {
      $respuesta = $this->peticion('PUT',"http://dsos.ito:81/api/eliminar_Api",[
        'headers' => [
           'Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'raza' => 'Poodle'
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
  }
}
?>
