<?php
namespace App\Models\ajax;
use Illuminate\Database\Eloquent\Model;

class modelo_cliente extends Model{
    protected $table = 'cliente';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = ['id', 'nombre','apellidos','direccion','rfc'];
}
 ?>
