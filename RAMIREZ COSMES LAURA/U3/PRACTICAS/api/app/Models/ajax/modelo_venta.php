<?php
namespace App\Models\ajax;
use Illuminate\Database\Eloquent\Model;

class modelo_venta extends Model{
    protected $table = 'venta';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = ['id', 'software','precio','licencia','id_cliente'];
}
 ?>
