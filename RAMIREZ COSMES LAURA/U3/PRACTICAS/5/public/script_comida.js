$(function(){
  $('#idselector1').on('change',metodo_listar);

});

function metodo_listar(){
    var tipo =document.getElementById('idselector1').value;
    $.get('tipos/'+tipo+'', function(data){
        var html_select ='<option value="">SELECCIONE UNA OPCION</option>';

        for(var i=0; i < data.length; i++)
            html_select += '<option value="'+data[i].nombre_comida+'">'
            +data[i].nombre_comida+'</option>'

        $('#idselector').html(html_select);
        $('#idselector').on('change',metodo_precio);
        //console.log(html_select);
    });
}

function metodo_precio(){
    var comida = $('select[id=idselector]').val();
    //console.log(comida);
    $.get('precio/'+comida+'', function(data){
        var html_selectp = data.costo;
        $('#idselector2').val(html_selectp);
        //console.log(html_selectp);
    });
    $('#cantidad').on('change',calcular_precio);
}

function calcular_precio(){
    var precio = $('#idselector2').val();
    //console.log("precio funcion "+precio);
    var cant = $('#cantidad').val();
    //console.log("cant funcion "+cant);
    var precio_total = precio * cant;
    $('#preciot').val(precio_total);
    //console.log(precio_total);
}
