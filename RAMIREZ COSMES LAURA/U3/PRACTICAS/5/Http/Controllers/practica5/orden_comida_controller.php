<?php
namespace App\Http\Controllers\practica5;
use App\Http\Controllers\Controller;
use App\Models\practica5\modelo_orden_comida;
use App\Models\practica5\modelo_comida;
use Illuminate\Http\Request;
use Iluminate\Support\Facades\Input;

class orden_comida_controller extends Controller
{
    public function ver_pedidos(){
        $tabla = modelo_orden_comida::all();
        return view('practica5\practica5vista')->with('x',$tabla);
    }

    public function listar_tipo($tipo){
        $consulta =modelo_comida::select('id','nombre_comida','tipo','costo')
        ->where('tipo', $tipo)->get();
        return $consulta;
    }

    public function seleccion_tipo(){
        $ver1=modelo_comida::pluck('tipo','tipo');
        return view('practica5/practica5')
        ->with('lista_t',$ver1);
    }

    public function dar_precio($comida){
      $consulta =modelo_comida::select('id','nombre_comida','costo')
      ->where('nombre_comida', $comida)->take(1)->first();
      return $consulta;
    }

    public function insertar_form(Request $request){
        $nombre =$request->input('comida');
        $cant=$request->input('cantidad');
        $costo=$request->input('preciot');
        modelo_orden_comida::create(['nombre_comida'=>$nombre,'cantidad'=>$cant,
        'costo_total'=>$costo]);
        return redirect()->to('\orden_principal');
    }
}
?>
