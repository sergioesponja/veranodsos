<?php
namespace App\Models\practica5;
use Illuminate\Database\Eloquent\Model;

class modelo_comida extends Model
{    
      protected $table ='comida';
      protected $primarykey='id';
      public $timestamps=false;
      protected $fillable =['id','nombre_comida','tipo','costo'];
}
?>
