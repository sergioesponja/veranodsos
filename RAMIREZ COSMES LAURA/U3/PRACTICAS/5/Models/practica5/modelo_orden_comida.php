<?php
namespace App\Models\practica5;
use Illuminate\Database\Eloquent\Model;

class modelo_orden_comida extends Model{
    protected $table = 'orden';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = ['id', 'nombre_comida','cantidad','costo_total'];
}
 ?>
