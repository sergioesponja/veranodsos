<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Pedidos</title>
</head>
<body>
    <table>
        <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Total</th>
        </thead>
        @foreach ($x as $c)
        <tr>
            <td>{{ $c -> id}}</td>
            <td>{{ $c -> nombre_comida}}</td>
            <td>{{ $c -> cantidad}}</td>
            <td>{{ $c -> costo_total}}</td>
        </tr>
        @endforeach

        <tr>
            <td>Insertar: <a href ="/orden"> Insertar </a></td>
        </tr>
    </table>
</body>
</html>
