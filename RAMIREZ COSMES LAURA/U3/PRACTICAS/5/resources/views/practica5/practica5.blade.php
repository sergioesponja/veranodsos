<!DOCTYPE html>
<html lang="es">
  <head>
          <meta charset="utf-8">
          <script src="js/jquery.min.js"></script>
          <script src="js/script_comida.js"></script>
          <title>Nuevo pedido </title>

  </head>
  <body>
      <h2>Pedido</h2>
      <br>
        {!!Form::open(array('url'=>'agregar_orden','method'=>'POST','autocomplete'=>'off'))!!}
        Tipo: {{Form::select('tipo',$lista_t,null,['id'=>'idselector1',
        'placeholder'=>'SELECCIONE TIPO'])}}
        <br><br>
        {!!Form::label('Producto: ')!!}<span style="color:red">*</span>
        <select name='comida' id='idselector'>
            <option value ="">SELECCIONE UNA OPCION</option>
        </select>
        <br><br>
        {!!Form::label('Precio: ')!!}
        {!!Form::text('idselector2',null,['id' => 'idselector2','class'=>'form-control',
        'placeholder'=>''])!!}
        <br><br>
        {!!Form::label('Cantidad')!!}<span style="color:red">*</span>
        {!!Form::text('cantidad',null,['id' => 'cantidad','class'=>'form-control',
        'placeholder'=>''])!!}
        <br><br>
        {!!Form::label('Precio Total: ')!!}
        {!!Form::text('preciot',null,['id' => 'preciot','class'=>'form-control',
        'placeholder'=>''])!!}
        <br><br>
      {!!form::submit('Registrar Orden',['name'=>'grabar','content'=>'<span>Registrar</span>','class'=>'btn btn=primary'])!!}
      {!!Form::close()!!}

  </body>
  </html>
