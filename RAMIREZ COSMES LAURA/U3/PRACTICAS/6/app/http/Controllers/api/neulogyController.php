<?php
namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Iluminate\Support\Facades\Input;
use App\Models\ajax\modelo_venta;
use App\Models\ajax\modelo_cliente;

class neulogyController extends Controller
{
    public function insertar_api(Request $var){
        $nombre = $var->nombrev;
        $apellido = $var->apellidov;
        $direccion = $var->direccionv;
        $rfc = $var->rfcv;
        modelo_cliente::create(['nombre' => $nombre, 'apellidos' => $apellido,
        'direccion' => $direccion, 'rfc' => $rfc,]);
        return response()->json(['respuesta' => 'Registrado correctamente']);
    }

    public function insertar_venta(Request $var){
        $nombre = $var->nombrev;
        $apellido = $var->apellidov;
        $direccion = $var->direccionv;
        $rfc = $var->rfcv;
        modelo_cliente::create(['nombre' => $nombre, 'apellidos' => $apellido,
        'direccion' => $direccion, 'rfc' => $rfc,]);
        return response()->json(['respuesta' => 'Registrado correctamente']);
    }

    public function ver_ventas_cliente(Request $idv){
        $id = $idv -> idv;
        $consulta_ver = modelo_venta::select('id','software','precio','licencia')
        ->where('id_cliente',$id)->get();
        return $consulta_ver;
    }

    public function eliminar_ventas(Request $request){
        $id = $request -> idv;
        modelo_venta::where('id_cliente', $id)->delete();
        return response()->json(['respuesta' => 'Eliminado correctamente']);
    }

    public function actualizar_venta(Request $val){
        $id = $val->idv;
        $actualizar = modelo_venta::find($id);
        $actualizar->software = $val->nombrev;
        $actualizar->precio = $val->preciov;
        $actualizar->licencia = $val->licenciav;
        $actualizar->id_cliente = $val->clientev;
        $actualizar->save();
        return response()->json(['respuesta' => 'Actualizado correctamente']);
    }



}
