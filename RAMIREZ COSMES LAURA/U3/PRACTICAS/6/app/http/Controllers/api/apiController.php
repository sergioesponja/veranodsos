<?php
namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Iluminate\Support\Facades\Input;
use App\Models\ajax\modelo_perro;

class apiController extends Controller
{
    public function ver_api(){
        $raza = $_REQUEST["raza"];
        $consulta_ver = modelo_perro::select('nombre','raza')
        ->where('raza',$raza)->get();
        return $consulta_ver;
    }

    public function ver_api_post(Request $request){
        $raza = $request ->input('tipo_raza');
        $consulta_ver = modelo_perro::select('nombre','raza')
        ->where('raza',$raza)->get();
        return $consulta_ver;
    }

    public function insertar_api(Request $var){
        $nombre = $var->nombrev;
        $raza = $var->razav;
        modelo_perro::create(['nombre' => $nombre, 'raza' => $raza]);
        return response()->json(['respuesta' => 'Registrado correctamente']);
    }

    public function actualizar_api(Request $val){
        $id = $val->idv;
        $actualizar = modelo_perro::find($id);
        $actualizar->nombre = $val->nombrev;
        $actualizar->raza = $val->razav;
        $actualizar->save();
        return response()->json(['respuesta' => 'Actualizado correctamente']);
    }

    public function eliminar_api(Request $request){
        $id = $request -> nombrev;
        $eliminar = modelo_perro::find($id);
        $eliminar -> delete();
        return response()->json(['respuesta' => 'Eliminado correctamente']);
    }

}
?>
