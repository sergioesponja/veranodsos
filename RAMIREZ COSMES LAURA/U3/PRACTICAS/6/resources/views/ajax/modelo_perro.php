<?php
namespace App\Models\ajax;
use Illuminate\Database\Eloquent\Model;

class modelo_perro extends Model{
    protected $table = 'perro';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $fillable = ['id', 'nombre','raza'];
}
 ?>
