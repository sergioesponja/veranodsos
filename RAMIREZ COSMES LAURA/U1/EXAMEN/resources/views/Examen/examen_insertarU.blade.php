<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Insertar Datos</title>
</head>
<body>
    <p>Insertar bienes</p>

    {!!Form::open(array('url'=>'Examen/insertar_formU', 'method'=>'POST', 'autocomplete'=>'off'))!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', null, ['id'=>'nombre', 'class'=>'form-control',
        'placeholder'=>'Ingrese nombre'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Usuario')!!}<span style="color:red">*</span>
        {!!Form::text('usuario', null, ['id'=>'usuario', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Contraseña')!!}
        {!!Form::text('pass', null, ['id'=>'pass', 'class'=>'form-control'])!!}
    </div>

    {!!Form::submit('Registrar', ['id'=>'insertar_formU',
    'content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}
</body>
</html>
