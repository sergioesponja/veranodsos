<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Insertar Datos</title>
</head>
<body>
    <p>Insertar videojuegos</p>

    {!!Form::open(array('url'=>'Examen/insertar_formV', 'method'=>'POST', 'autocomplete'=>'off'))!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', null, ['id'=>'nombre', 'class'=>'form-control',
        'placeholder'=>'Ingrese nombre'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Tipo')!!}<span style="color:red"></span>
        {!!Form::text('tipo', null, ['id'=>'tipo', 'class'=>'form-control'])!!}
    </div>

    {!!Form::submit('Registrar', ['id'=>'insertar_formV',
    'content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}
</body>
</html>
