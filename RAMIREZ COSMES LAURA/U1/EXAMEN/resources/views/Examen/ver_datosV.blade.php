<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Editar Datos</title>
</head>
<body>
    <p>Editar Videojuegos</p>

    {!!Form::open(array('url'=>'Examen/mostrar_todoV', 'method'=>'PUT', 'autocomplete'=>'off'))!!}
    {!!form::hidden('id',$modificar->id, ['id'=>'id'])!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', $modificar->nombre, ['id'=>'nombre', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Tipo')!!}
        {!!Form::text('tipo', $modificar->tipo, ['id'=>'tipo', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Status')!!}
        {!!Form::text('status', $modificar->status, ['id'=>'status', 'class'=>'form-control'])!!}
    </div>
    {!!Form::submit('Actualizar', ['id'=>'insertar_form',
    'content'=>'<span>Actualizar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

</body>
</html>
