<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Videojuegos</title>
</head>
<body>
    <a>Videojuegos Disponibles</a>
    <table>
        <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Tipo</th>
        </thead>
        @foreach ($x as $c)
        <tr>
            <td>{{ $c -> id}}</td>
            <td>{{ $c -> nombre}}</td>
            <td>{{ $c -> tipo}}</td>
            <td>
                <a href ="/Examen/editarV/{{ $c -> id}}"> Editar </a>
                <a href ="/Examen/deleteV/{{ $c -> id}}"> Eliminar </a>
                <a href ="/Examen/bajaV/{{ $c -> id}}"> Baja </a>
            </td>
        </tr>
        @endforeach

        <tr>
            <td>Insertar: <a href ="/Examen/insertarV/"> Insertar </a></td>
        </tr>
        <tr>
            <td>Alta: <a href ="/Examen/buscar_altaV/"> Dar de Alta </a></td>
        </tr>
        <tr>
            <td>Ver todos los datos: <a href ="/Examen/ver_todosV/"> Datos </a></td>
        </tr>
        <tr>
            <td>Ver Usuarios: <a href ="/Examen/ver_todoU/"> Usuarios </a></td>
        </tr>
    </table>
</body>
</html>
