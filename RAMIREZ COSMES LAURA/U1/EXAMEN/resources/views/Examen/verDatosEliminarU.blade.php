<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Eliminar Uusuario</title>
</head>
<body>
    <p>Eliminar</p>

    {!!Form::open(array('url'=>'Examen/eliminarU', 'method'=>'GET', 'autocomplete'=>'off'))!!}

    {!!form::hidden('buscarid',$eliminar->id, ['id'=>'id'])!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', $eliminar->nombre, ['id'=>'nombre', 'class'=>'form-control', 'readonly'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Usuario')!!}
        {!!Form::text('usuario', $eliminar->usuario, ['id'=>'descripcion', 'class'=>'form-control'])!!}
    </div>
    {!!Form::submit('Eliminar', ['id'=>'eliminar',
    'content'=>'<span>Eliminar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

</body>
</html>
