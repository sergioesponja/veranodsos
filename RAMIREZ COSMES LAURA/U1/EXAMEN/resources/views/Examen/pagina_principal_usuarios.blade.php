<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>usuarios</title>
</head>
<body>
    <a>Usuarios</a>
    <table>
        <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Usuario</th>
        </thead>
        @foreach ($x as $c)
        <tr>
            <td>{{ $c -> id}}</td>
            <td>{{ $c -> nombre}}</td>
            <td>{{ $c -> usuario}}</td>
            <td>
                <a href ="/Examen/editarU/{{ $c -> id}}"> Editar </a>
                <a href ="/Examen/deleteU/{{ $c -> id}}"> Eliminar </a>
                <a href ="/Examen/bajaU/{{ $c -> id}}"> Baja </a>
            </td>
        </tr>
        @endforeach

        <tr>
            <td>Insertar: <a href ="/Examen/insertarU/"> Insertar </a></td>
        </tr>
        <tr>
            <td>Alta: <a href ="/Examen/buscar_altaU/"> Dar de Alta </a></td>
        </tr>
        
        <tr>
            <td>Ver Videojuegos: <a href ="/Examen/ver_todo"> Videojuegos </a></td>
        </tr>
    </table>
</body>
</html>
