<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Alta</title>
</head>
<body>
    <p>Alta</p>

    {!!Form::open(array('url'=>'Examen/alta_formU', 'method'=>'GET', 'autocomplete'=>'off'))!!}

    {!!form::hidden('id',$alta->id, ['id'=>'id'])!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', $alta->nombre, ['id'=>'nombre', 'class'=>'form-control', 'readonly'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Usuario')!!}
        {!!Form::text('usuario', $alta->usuario, ['id'=>'usuario', 'class'=>'form-control'])!!}
    </div>
    {!!Form::submit('alta', ['id'=>'alta',
    'content'=>'<span>Dar de Alta</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

</body>
</html>
