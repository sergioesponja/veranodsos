<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Tabla 2</title>
</head>
<body>
    <p>Tabla</p>

    {!!Form::open(array('url'=>'Examen/baja_formU', 'method'=>'GET', 'autocomplete'=>'off'))!!}
    {!!form::hidden('buscarid',$baja->id, ['id'=>'id'])!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', $baja->nombre, ['id'=>'nombre', 'class'=>'form-control', 'readonly'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Usuario')!!}
        {!!Form::text('usuario', $baja->usuario, ['id'=>'usuario', 'class'=>'form-control'])!!}
    </div>
    {!!Form::submit('baja', ['id'=>'baja',
    'content'=>'<span>Dar de Baja</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

</body>
</html>
