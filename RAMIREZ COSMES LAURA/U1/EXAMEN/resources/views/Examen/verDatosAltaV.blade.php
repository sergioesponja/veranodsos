<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Tabla 2</title>
</head>
<body>
    <p>Tabla</p>

    {!!Form::open(array('url'=>'Examen/alta_formV', 'method'=>'GET', 'autocomplete'=>'off'))!!}

    {!!form::hidden('id',$alta->id, ['id'=>'id'])!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', $alta->nombre, ['id'=>'nombre', 'class'=>'form-control', 'readonly'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Tipo')!!}
        {!!Form::text('tipo', $alta->tipo, ['id'=>'tipo', 'class'=>'form-control'])!!}
    </div>
    {!!Form::submit('alta', ['id'=>'alta',
    'content'=>'<span>Dar de Alta</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

</body>
</html>
