<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Alta videojuegos</title>
</head>
<body>
    <p>Dar de Alta</p>

    {!!Form::open(array('url'=>'Examen/altaV', 'method'=>'POST', 'autocomplete'=>'off'))!!}

    <div class="form-group">
        {!!Form::label('ID Alta')!!}<span style="color:red">*</span>
        {!!Form::text('buscarid', null, ['id'=>'buscarid', 'class'=>'form-control',
        'placeholder'=>'Ingrese ID existente'])!!}
    </div>

    {!!Form::submit('Buscar', ['id'=>'grabar',
    'content'=>'<span>Buscar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}
</body>
</html>
