<?php
namespace App\Http\Controllers\Examen;
use App\Http\Controllers\Controller;
use App\Models\Examen\examenUsuariosModelo;
use App\Models\Examen\examenVideojuegosModelo;
use App\Models\Examen\examenUsuariosM;
use App\Models\Examen\examenVideojuegosM;
use Illuminate\Http\Request;
use Iluminate\Support\Facades\Input;

class examenController extends Controller
{
    public function ver_todo(){
        $tabla = examenVideojuegosM::all();
        $tabla = examenVideojuegosM::where('status', "1")->get();
        return view('Examen\pagina_principal')->with('x',$tabla);
    }

    public function ver_todoU(){
        $tabla = examenUsuariosM::all();
        $tabla = examenUsuariosM::where('status', "1")->get();
        return view('Examen\pagina_principal_usuarios')->with('x',$tabla);
    }

    public function insertarV(){
        return view('Examen\examen_insertarV');
    }

    public function insertar_formV(Request $request){
        $nombre = $request->input('nombre');
        $tipo = $request->input('tipo');

        examenVideojuegosM::create(['nombre'=> $nombre, 'tipo'=>$tipo]);
        return redirect()->to('Examen\ver_todo');
    }

    public function insertarU(){
        return view('Examen\examen_insertarU');
    }

    public function insertar_formU(Request $request){
        $nombre = $request->input('nombre');
        $usuario = $request->input('usuario');
        $pass = $request->input('pass');

        examenUsuariosModelo::create(['nombre'=> $nombre, 'usuario'=>$usuario, 'pass'=> $pass]);
        return redirect()->to('Examen\ver_todoU');
    }

    public function editar_todoV($id){
        $actualizar = examenVideojuegosModelo::find($id);
        return view ('Examen\ver_datosV')-> with('modificar', $actualizar);
    }

    public function actualizar_todoV(Request $request){
        $id = $request->input('id');
        $actualizar = examenVideojuegosModelo::find($id);
        $actualizar->nombre = $request->nombre;
        $actualizar->tipo = $request->tipo;
        $actualizar->status = $request->status;
        $actualizar->save();
        return redirect()->to('Examen\ver_todo');
    }

    public function editar_todoU($id){
        $actualizar = examenUsuariosModelo::find($id);
        return view ('Examen\ver_datosU')-> with('modificar', $actualizar);
    }

    public function actualizar_todoU(Request $request){
        $id = $request->input('id');
        $actualizar = examenUsuariosModelo::find($id);
        $actualizar->nombre = $request->nombre;
        $actualizar->pass = $request->pass;
        $actualizar->status = $request->status;
        $actualizar->save();
        return redirect()->to('Examen\ver_todoU');
    }

    public function ver_datos_bajaV($id){
        $consulta = examenVideojuegosM::where('id',$id)->take(1)->first();
        return view('Examen\verDatosBajaV')->with('baja',$consulta);
    }

    public function bajaV(Request $request){
        $id = $request -> input('buscarid');
        $baja = examenVideojuegosM::find($id);
        $baja->status = '0';
        $baja->save();
        return redirect()->to('Examen\ver_todo');
    }

    public function ver_datos_bajaU($id){
        $consulta = examenUsuariosM::where('id',$id)->take(1)->first();
        return view('Examen\verDatosBajaU')->with('baja',$consulta);
    }

    public function bajaU(Request $request){
        $id = $request -> input('buscarid');
        $baja = examenUsuariosM::find($id);
        $baja->status = '0';
        $baja->save();
        return redirect()->to('Examen\ver_todoU');
    }

    public function buscar_altaV(){
        return view('Examen\buscarAltaV');
    }

    public function ver_datos_altaV(Request $request){
        $id = $request -> input('buscarid');
        $consulta = examenVideojuegosM::where('id', $id)->take(1)->first();
        return view('Examen\verDatosAltaV')->with('alta',$consulta);
    }

    public function altaV(Request $request){
        $id = $request->input('id');
        $alta = examenVideojuegosM::find($id);
        $alta->status = '1';
        $alta->save();
        return redirect()->to('Examen\ver_todo');
    }

    public function buscar_altaU(){
        return view('Examen\buscarAltaU');
    }

    public function ver_datos_altaU(Request $request){
        $id = $request -> input('buscarid');
        $consulta = examenUsuariosM::where('id', $id)->take(1)->first();
        return view('Examen\verDatosAltaU')->with('alta',$consulta);
    }

    public function altaU(Request $request){
        $id = $request->input('id');
        $alta = examenUsuariosModelo::find($id);
        $alta->status = '1';
        $alta->save();
        return redirect()->to('Examen\ver_todoU');
    }

    public function ver_datos_eliminarV($id){
        $consulta = examenVideojuegosM::where('id',$id)->take(1)->first();
        return view('Examen\verDatosEliminarV')->with('eliminar',$consulta);
    }

    public function eliminarV(Request $request){
        $id = $request -> input('buscarid');
        $eliminar = examenVideojuegosM::find($id);
        $eliminar -> delete();
        return redirect()->to('Examen\ver_todo');
    }

    public function ver_datos_eliminarU($id){
        $consulta = examenUsuariosM::where('id',$id)->take(1)->first();
        return view('Examen\verDatosEliminarU')->with('eliminar',$consulta);
    }

    public function eliminarU(Request $request){
        $id = $request -> input('buscarid');
        $eliminar = examenUsuariosM::find($id);
        $eliminar -> delete();
        return redirect()->to('Examen\ver_todoU');
    }

    public function verTodosV(){
        $tabla = examenVideojuegosM::all();
        return view('Examen\pagina_principal')->with('x',   $tabla);
    }

}





 ?>
