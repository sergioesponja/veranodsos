<?php
    namespace App\Models\Examen;
    use Illuminate\Database\Eloquent\Model;

    class examenUsuariosModelo extends Model
    {
        protected $table = 'empleados';
        protected $primarykey = 'id';
        public $incrementing = true;
        public $timestamps = false;
        protected $fillable = ['id', 'nombre', 'status', 'usuario', 'pass'];
    }
 ?>
