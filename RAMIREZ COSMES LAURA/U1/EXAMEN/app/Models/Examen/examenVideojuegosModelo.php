<?php
    namespace App\Models\Examen;
    use Illuminate\Database\Eloquent\Model;

    class examenVideojuegosModelo extends Model
    {
        protected $table = 'videojuegos';
        protected $primarykey = 'id';
        public $incrementing = true;
        public $timestamps = false;
        protected $fillable = ['id', 'nombre', 'tipo', 'status'];
    }
 ?>
