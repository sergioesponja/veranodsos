<?php
    namespace App\Models\Examen;
    use Illuminate\Database\Eloquent\Model;

    class examenUsuariosM extends Model
    {
        protected $table = 'empleados';
        protected $primarykey = 'id';
        public $incrementing = true;
        public $timestamps = false;
        protected $fillable = ['id', 'nombre', 'usuario'];
    }
 ?>
