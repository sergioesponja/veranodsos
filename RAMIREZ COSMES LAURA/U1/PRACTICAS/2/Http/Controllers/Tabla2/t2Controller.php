<?php
namespace App\Http\Controllers\Tabla2;
use App\Http\Controllers\Controller;
use App\Models\Tabla2\t2Modelo;
use App\Models\Tabla2\t2Insertar;
use App\Models\Tabla2\t2Contrasena;

use Illuminate\Http\Request;
use Iluminate\Support\Facades\Input;

class t2Controller extends Controller
{
    public function verConsulta()
    {
        $consulta = t2Modelo::all();
        return $consulta;
    }

    public function insertar(){
        return view('Tabla2/t2vista');
    }

    public function insertar_form(Request $request){
        $nombre = $request->input('nombre');
        $apellido1 = $request->input('apellido1');
        $apellido2 = $request->input('apellido2');
        $usuario = $request -> input('usuario');
        $pass = $request-> input('contraseña');

        t2Insertar::create(['nombre'=> $nombre, 'apellido1'=>$apellido1,
        'apellido2'=>$apellido2, 'usuario'=>$usuario, 'pass'=>$pass]);

        return "registrado correctamente";
    }

    public function eliminar_busqueda(){
        return view('Tabla2\buscarID');
    }

    public function eliminar(Request $request){
        $id = $request -> input('buscarid');
        $eliminar = t2Modelo::find($id);
        $eliminar -> delete();

        return "eliminado correctamente";
    }

    public function act_busqueda(){
        return view('Tabla2\buscart2');
    }

    public function verDatos(Request $request){
        $id = $request -> input('busqueda');
        $consulta = t2Modelo::where('id', $id)->take(1)->first();
        return view('Tabla2\verDatos')->with('modificar',$consulta);
    }

    public function actualizarok(Request $request){
        $id = $request->input('id');
        $actualizar = t2Modelo::find($id);
        $actualizar->nombre = $request->nombre;
        $actualizar->apellido1 = $request->apellido1;
        $actualizar->apellido2 = $request->apellido2;
        $actualizar->usuario = $request->usuario;
        $actualizar->save();

        return 'Actualizado correctamente';
    }

    public function act_contraseña(Request $request){
        $id = $request -> input('id');
        $consulta = t2Contrasena::where('id', $id)->take(1)->first();
        return view('Tabla2\verContraseñac')->with('modificar',$consulta);
    }

    public function actualizarContraseña(Request $request){
        $id = $request->input('id');
        $actualizar = t2Contrasena::find($id);
        $actualizar->pass = $request->contrasena;
        $actualizar->save();

        return 'Contraseña Actualizada correctamente';
    }
}
?>
