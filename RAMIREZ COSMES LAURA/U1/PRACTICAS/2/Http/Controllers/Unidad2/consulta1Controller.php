<?php
    namespace App\Http\Controllers\Unidad2;
    use App\Http\Controllers\Controller;
    use App\Models\Unidad2\consulta1Modelo;

    class consulta1Controller extends Controller
    {

        public function ver_consulta()
        {
            $consulta = consulta1Modelo::all();
            return $consulta;
        }

        public function insertar(){
            $insertar = consulta1Modelo::create(['rfc' => 'CUJI890802QV3']);
            return 'registrado correctamente';  
        }
    }

 ?>
