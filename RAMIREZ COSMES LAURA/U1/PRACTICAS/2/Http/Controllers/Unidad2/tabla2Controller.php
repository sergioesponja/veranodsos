<?php
    namespace App\Http\Controllers\Unidad2;
    use App\Http\Controllers\Controller;
    use App\Models\Unidad2\tabla2Modelo;

    use App\Models\Unidad2\insertarT2;
    use Illuminate\Http\Request;
    use Iluminate\Support\Facades\Input;

    class tabla2Controller extends Controller
    {
        public function vistaT2(){
            return view('Practica1/vistaT2');
        }
        public function ver_consulta()
        {
            $consulta = tabla2Modelo::all();
            return $consulta;
        }

        public function insertar(){
            return view('Practica1/vistaT2');
        }

        public function insertar_form(Request $request){
            $nombre = $request->input('nombres');
            $apellido1 = $request->input('apellido1');
            $apellido2 = $request->input('apellido2');
            $usuario = $request -> input('usuario');
            $pass = $request-> input('contraseña');

            insertarT2::create(['nombre'=> $nombre, 'apellido1'=>$apellido1,
            'apellido2'=>$apellido2, 'usuario'=>$usuario, 'pass'=>$pass]);

            return "registrado correctamente";
        }
    }

 ?>
