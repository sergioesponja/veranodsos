<?php
namespace App\Models\Unidad2;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class insertarT2 extends AnotherClass
{

    protected $table = 'tabla1';
    protected $primarykey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['nombre', 'apellido1','apellido2','usuario', 'pass']
}

 ?>
