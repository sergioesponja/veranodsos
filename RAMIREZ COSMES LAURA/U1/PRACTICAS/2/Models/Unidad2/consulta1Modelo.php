<?php
    namespace App\Models\Unidad2;
    use Illuminate\Database\Eloquent\Model;

    /**
     *
     */
    class consulta1Modelo extends Model
    {
        protected $table = 'users';
        protected $primarykey = 'id';
        public $incrementing = true;
        public $timestamps = false;

        protected $fillable = ['id', 'rfc'];
    }

 ?>
