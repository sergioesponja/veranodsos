<?php
    namespace App\Models\Unidad2;
    use Illuminate\Database\Eloquent\Model;

    /**
     *
     */
    class tabla2Modelo extends Model
    {
        protected $table = 'tabla1';
        protected $primarykey = 'id';
        public $incrementing = true;
        public $timestamps = false;

        protected $fillable = ['nombre', 'apellido1', 'apellido2', 'usuario'];
    }

 ?>
