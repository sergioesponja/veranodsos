<?php
    namespace App\Models\Tabla2;
    use Illuminate\Database\Eloquent\Model;

    class t2Modelo extends Model
    {
        protected $table = 'tabla1';
        protected $primarykey = 'id';
        public $incrementing = true;
        public $timestamps = false;

        protected $fillable = ['id', 'nombre', 'apellido1', 'apellido2', 'usuario'];
    }
 ?>
