<?php
namespace App\Models\Tabla2;
use Illuminate\Database\Eloquent\Model;

class t2Insertar extends Model
{
    protected $table = 'tabla1';
    protected $primarykey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['nombre', 'apellido1','apellido2','usuario', 'pass'];
}
?>
