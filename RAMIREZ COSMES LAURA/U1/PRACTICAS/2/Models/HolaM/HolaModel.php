<?php
namespace App\Models\HolaM;

use Iluminate\Database\Eloquent\Model;

class HomaModel extends Model{
    protected $table = 'users';

    protected $primarykey = 'rfc';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'rfc', 'nombre' ];
}
 ?>
