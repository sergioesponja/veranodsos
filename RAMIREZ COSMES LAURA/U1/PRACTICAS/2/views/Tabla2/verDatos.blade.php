<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Tabla 2</title>
</head>
<body>
    <p>Tabla</p>

    {!!Form::open(array('url'=>'mostrarBusqueda', 'method'=>'PUT', 'autocomplete'=>'off'))!!}

    {!!form::hidden('id',$modificar->id, ['id'=>'id'])!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', $modificar->nombre, ['id'=>'nombre', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Apellido paterno')!!}<span style="color:red">*</span>
        {!!Form::text('apellido1', $modificar->apellido1, ['id'=>'apellido1', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Apellido materno')!!}
        {!!Form::text('apellido2', $modificar->apellido2, ['id'=>'apellido2', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Usuario')!!}
        {!!Form::text('usuario', $modificar->usuario, ['id'=>'usuario', 'class'=>'form-control'])!!}
    </div>
    {!!Form::submit('Actualizar', ['id'=>'insertar_form',
    'content'=>'<span>Actualizar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

    {!!Form::open(array('url'=>'cambiarContraseña', 'method'=>'POST', 'autocomplete'=>'off'))!!}
    {!!form::hidden('id',$modificar->id, ['id'=>'id'])!!}
    {!!Form::submit('Modificar Contraseña', ['id'=>'insertar_form',
    'content'=>'<span>ModificarC</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

</body>
</html>
