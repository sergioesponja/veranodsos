<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Tabla 2</title>
</head>
<body>
    <p>Tabla</p>

    {!!Form::open(array('url'=>'insertar_formt2', 'method'=>'POST', 'autocomplete'=>'off'))!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', null, ['id'=>'nombre', 'class'=>'form-control',
        'placeholder'=>'Ingrese nombre(s)'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Apellido paterno')!!}<span style="color:red">*</span>
        {!!Form::text('apellido1', null, ['id'=>'apellido1', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Apellido materno')!!}
        {!!Form::text('apellido2', null, ['id'=>'apellido2', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Usuario')!!}
        {!!Form::text('usuario', null, ['id'=>'usuario', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Contraseña')!!}
        {!!Form::text('contraseña', null, ['id'=>'pass', 'class'=>'form-control'])!!}
    </div>

    {!!Form::submit('Registrar', ['id'=>'insertar_form',
    'content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}
</body>
</html>
