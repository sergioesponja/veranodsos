<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Tabla 2</title>
</head>
<body>
    <p>Tabla</p>

    {!!Form::open(array('url'=>'mostrarC', 'method'=>'PUT', 'autocomplete'=>'off'))!!}

    {!!form::hidden('id',$modificar->id, ['id'=>'id'])!!}

    <div class="form-group">
        {!!Form::label('Contraseña')!!}<span style="color:red">*</span>
        {!!Form::text('contrasena', $modificar->contrasena, ['id'=>'pass', 'class'=>'form-control'])!!}
    </div>

    {!!Form::submit('Actualizar', ['id'=>'insertar_form',
    'content'=>'<span>Actualizar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}
</body>
</html>
