<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <title>Practica 1</title>
    </head>
    <body>
        <p>Tabla</p>

        <table class="tabla" border="1px solid blue">
            <tr>
                <th>id</th>
                <th>Nombre</th>
                <th>Edad</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Iván</td>
                <td>29</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Adán</td>
                <td>30</td>
            </tr>
        </table>
    </body>
</html>
