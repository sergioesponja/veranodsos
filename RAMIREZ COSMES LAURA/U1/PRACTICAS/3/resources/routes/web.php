<?php
Auth::routes();

Route::get('x/z/y','Unidad2\HolaMundoController@texto');

Route::get('practica1','Practica1\vistaTablaController@practicaT');

Route::get('consulta1','Unidad2\consulta1Controller@ver_consulta');

Route::get('insertar','Unidad2\consulta1Controller@insertar');

//Route::get('vistaT','Unidad2\tabla2Controller@vistaT2');
//Route::get('consultaT2','Unidad2\tabla2Controller@ver_consulta');
//Route::get('insertarT2','Unidad2\tabla2Controller@insertar');
//Route::get('insertar_formT2','Unidad2\tabla2Controller@insertar_form')

Route::get('consultat2','Tabla2\t2Controller@verConsulta');
Route::get('insertart2','Tabla2\t2Controller@insertar');
Route::post('insertar_formt2','Tabla2\t2Controller@insertar_form');

Route::get('eliminar_bt2','Tabla2\t2Controller@eliminar_busqueda');
Route::get('eliminart2','Tabla2\t2Controller@eliminar');

Route::get('buscart2','Tabla2\t2Controller@act_busqueda');
Route::post('busquedat2','Tabla2\t2Controller@verDatos');
Route::put('mostrarBusqueda','Tabla2\t2Controller@actualizarok');

Route::post('cambiarContraseña','Tabla2\t2Controller@act_contraseña');
Route::put('mostrarC','Tabla2\t2Controller@actualizarContraseña');

Route::get('ver_todo','Tabla2\t2Controller@vertabla');
Route::get('editar/{id}','Tabla2\t2Controller@editar_todo');
Route::get('delete/{id}','Tabla2\t2Controller@ver_datos_eliminar');

Route::get('baja/{id}','Tabla2\t2Controller@ver_datos_baja');
Route::get('bajat2','Tabla2\t2Controller@baja');

Route::get('buscarAltat2','Tabla2\t2Controller@buscar_alta');
Route::post('altat2','Tabla2\t2Controller@ver_datos_alta');
Route::get('alta','Tabla2\t2Controller@alta');


Route::get('bienesM/ver_todo','bienesM\bienesController@ver_todo');
Route::get('bienesM/insertarM','bienesM\bienesController@insertar');
Route::post('bienesM/insertar_formM','bienesM\bienesController@insertar_form');
Route::get('bienesM/editarM/{id}','bienesM\bienesController@editar_todo');
Route::put('bienesM/mostrar_todo','bienesM\bienesController@actualizar_todo');
Route::get('bienesM/bajaM/{id}','bienesM\bienesController@ver_datos_baja');
Route::get('bienesM/baja_form','bienesM\bienesController@baja');
Route::get('bienesM/buscar_altaM','bienesM\bienesController@buscar_alta');
Route::post('bienesM/altaM','bienesM\bienesController@ver_datos_alta');
Route::get('bienesM/alta_form','bienesM\bienesController@alta');
Route::get('bienesM/ver_todosM','bienesM\bienesController@ver_todos');
Route::get('bienesM/deleteM/{id}','bienesM\bienesController@ver_datos_eliminar');
Route::get('bienesM/eliminar','bienesM\bienesController@eliminar');
//get = gethostnames
//post=insertamos
//put=actualizar y eliminar
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
