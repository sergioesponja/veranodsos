<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Insertar Datos</title>
</head>
<body>
    <p>Insertar bienes</p>

    {!!Form::open(array('url'=>'bienesM/insertar_formM', 'method'=>'POST', 'autocomplete'=>'off'))!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', null, ['id'=>'nombre', 'class'=>'form-control',
        'placeholder'=>'Ingrese nombre'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Descripción')!!}<span style="color:red">*</span>
        {!!Form::text('descripcion', null, ['id'=>'descripcion', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Fecha')!!}
        {!!Form::date('fecha', null, ['id'=>'fecha', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Cantidad')!!}
        {!!Form::text('cantidad', null, ['id'=>'cantidad', 'class'=>'form-control'])!!}
    </div>

    {!!Form::submit('Registrar', ['id'=>'insertar_form',
    'content'=>'<span>Registrar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}
</body>
</html>
