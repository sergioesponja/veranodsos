<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Editas Datos</title>
</head>
<body>
    <p>Editar Bienes</p>

    {!!Form::open(array('url'=>'bienesM/mostrar_todo', 'method'=>'PUT', 'autocomplete'=>'off'))!!}
    {!!form::hidden('id',$modificar->id, ['id'=>'id'])!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', $modificar->nombre, ['id'=>'nombre', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Descripción')!!}
        {!!Form::text('descripcion', $modificar->descripcion, ['id'=>'descripcion', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Fecha')!!}
        {!!Form::date('fecha', $modificar->fecha, ['id'=>'fecha', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Cantidad')!!}
        {!!Form::text('cantidad', $modificar->cantidad, ['id'=>'cantidad', 'class'=>'form-control'])!!}
    </div>
    {!!Form::submit('Actualizar', ['id'=>'insertar_form',
    'content'=>'<span>Actualizar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

</body>
</html>
