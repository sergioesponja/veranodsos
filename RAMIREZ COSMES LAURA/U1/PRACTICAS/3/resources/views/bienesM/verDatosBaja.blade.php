<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Tabla 2</title>
</head>
<body>
    <p>Tabla</p>

    {!!Form::open(array('url'=>'bienesM/baja_form', 'method'=>'GET', 'autocomplete'=>'off'))!!}
    {!!form::hidden('buscarid',$baja->id, ['id'=>'id'])!!}

    <div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', $baja->nombre, ['id'=>'nombre', 'class'=>'form-control', 'readonly'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Descripción')!!}
        {!!Form::text('descripcion', $baja->descripcion, ['id'=>'descripcion', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Fecha')!!}
        {!!Form::date('fecha', $baja->fecha, ['id'=>'fecha', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Cantidad')!!}
        {!!Form::text('cantidad', $baja->cantidad, ['id'=>'cantidad', 'class'=>'form-control'])!!}
    </div>
    {!!Form::submit('baja', ['id'=>'baja',
    'content'=>'<span>Dar de Baja</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

</body>
</html>
