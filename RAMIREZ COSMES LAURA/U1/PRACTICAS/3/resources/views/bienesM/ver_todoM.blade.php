<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Bienes Muebles</title>
</head>
<body>
    <table>
        <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Fecha</th>
            <th>Cantidad</th>
            <th>Estado</th>
        </thead>
        @foreach ($x as $c)
        <tr>
            <td>{{ $c -> id}}</td>
            <td>{{ $c -> nombre}}</td>
            <td>{{ $c -> descripcion}}</td>
            <td>{{ $c -> fecha}}</td>
            <td>{{ $c -> cantidad}}</td>
            <td>{{ $c -> estado}}</td>

            <td>
                <a href ="editarM/{{ $c -> id}}"> Editar </a>
                <a href ="deleteM/{{ $c -> id}}"> Eliminar </a>
                <a href ="bajaM/{{ $c -> id}}"> Baja </a>
            </td>
        </tr>
        @endforeach

        <tr>
            <td>Insertar: <a href ="insertarM/"> Insertar </a></td>
        </tr>
        <tr>
            <td>Alta: <a href ="buscar_altaM/"> Dar de Alta </a></td>
        </tr>
        <tr>
            <td>Ver todos los datos: <a href ="ver_todosM/"> Datos </a></td>
        </tr>
    </table>
</body>
</html>
