<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Tabla 2</title>
</head>
<body>
    <p>Tabla</p>

    {!!Form::open(array('url'=>'bienesM/eliminar', 'method'=>'GET', 'autocomplete'=>'off'))!!}

    {!!form::hidden('buscarid',$eliminar->id, ['id'=>'id'])!!}

    <<div class="form-group">
        {!!Form::label('Nombre')!!}<span style="color:red">*</span>
        {!!Form::text('nombre', $eliminar->nombre, ['id'=>'nombre', 'class'=>'form-control', 'readonly'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Descripción')!!}
        {!!Form::text('descripcion', $eliminar->descripcion, ['id'=>'descripcion', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Fecha')!!}
        {!!Form::date('fecha', $eliminar->fecha, ['id'=>'fecha', 'class'=>'form-control'])!!}
    </div>
    <div class="form-group">
        {!!Form::label('Cantidad')!!}
        {!!Form::text('cantidad', $eliminar->cantidad, ['id'=>'cantidad', 'class'=>'form-control'])!!}
    </div>
    {!!Form::submit('Eliminar', ['id'=>'eliminar',
    'content'=>'<span>Eliminar</span>','class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}

</body>
</html>
