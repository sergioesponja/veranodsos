<?php
namespace App\Http\Controllers\bienesM;
use App\Http\Controllers\Controller;
use App\Models\bienesM\bienesModelo;
use Illuminate\Http\Request;
use Iluminate\Support\Facades\Input;

class bienesController extends Controller
{
    public function ver_todo(){
        $tabla = bienesModelo::all();
        $tabla = bienesModelo::where('estado', "activo")->get();
        return view('bienesM\ver_todoM')->with('x',$tabla);
    }
    public function ver_todos(){
        $tabla = bienesModelo::all();
        return view('bienesM\ver_todoM')->with('x',$tabla);
    }

    public function insertar(){
        return view('bienesM\insertarM');
    }

    public function insertar_form(Request $request){
        $nombre = $request->input('nombre');
        $descripcion = $request->input('descripcion');
        $fecha = $request->input('fecha');
        $cantidad = $request -> input('cantidad');

        bienesModelo::create(['nombre'=> $nombre, 'descripcion'=>$descripcion,
        'fecha'=>$fecha, 'cantidad'=>$cantidad]);
        return redirect()->to('bienesM\ver_todo');
    }

    public function editar_todo($id){
        $actualizar = bienesModelo::find($id);
        return view ('bienesM\ver_datosM')-> with('modificar', $actualizar);
    }

    public function actualizar_todo(Request $request){
        $id = $request->input('id');
        $actualizar = bienesModelo::find($id);
        $actualizar->nombre = $request->nombre;
        $actualizar->descripcion = $request->descripcion;
        $actualizar->fecha = $request->fecha;
        $actualizar->cantidad = $request->cantidad;
        $actualizar->save();
        return redirect()->to('bienesM\ver_todo');
    }

    public function ver_datos_baja($id){
        $consulta = bienesModelo::where('id',$id)->take(1)->first();
        return view('bienesM\verDatosBaja')->with('baja',$consulta);
    }

    public function baja(Request $request){
        $id = $request -> input('buscarid');
        $baja = bienesModelo::find($id);
        $baja->estado = 'inactivo';
        $baja->save();
        return redirect()->to('bienesM\ver_todo');
    }

    public function buscar_alta(){
        return view('bienesM\buscarAlta');
    }

    public function ver_datos_alta(Request $request){
        $id = $request -> input('buscarid');
        $consulta = bienesModelo::where('id', $id)->take(1)->first();
        return view('bienesM\verDatosAlta')->with('alta',$consulta);
    }

    public function alta(Request $request){
        $id = $request->input('id');
        $alta = bienesModelo::find($id);
        $alta->estado = 'activo';
        $alta->save();
        return redirect()->to('bienesM\ver_todo');
    }

    public function ver_datos_eliminar($id){
        $consulta = bienesModelo::where('id',$id)->take(1)->first();
        return view('bienesM\verDatosEliminar')->with('eliminar',$consulta);
    }

    public function eliminar(Request $request){
        $id = $request -> input('buscarid');
        $eliminar = bienesModelo::find($id);
        $eliminar -> delete();
        return redirect()->to('bienesM\ver_todo');
    }

}





 ?>
