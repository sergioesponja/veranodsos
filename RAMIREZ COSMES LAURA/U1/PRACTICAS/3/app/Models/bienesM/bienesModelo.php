<?php
    namespace App\Models\bienesM;
    use Illuminate\Database\Eloquent\Model;

    class bienesModelo extends Model
    {
        protected $table = 'bienes_muebles';
        protected $primarykey = 'id';
        public $incrementing = true;
        public $timestamps = false;
        protected $fillable = ['id', 'nombre', 'descripcion', 'fecha', 'cantidad','estado'];
    }
 ?>
