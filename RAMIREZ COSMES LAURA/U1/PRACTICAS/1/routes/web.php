<?php
Auth::routes();

Route::get('x/z/y','Unidad2\HolaMundoController@texto');

Route::get('practica1','Practica1\vistaTablaController@practicaT');

//get = gethostnames
//post=insertamos
//put=actualizar y eliminar
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
