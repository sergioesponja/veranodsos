<?php
namespace App\Http\Controllers\unidad3\practica6;
use App\Http\Controllers\Controller;
use App\Models\unidad3\Ajax\Perro;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use GuezzleHttp\Client;

class ConsumoController extends Controller
{

 public function consumo_busqueda()
 {
 
   $respuesta = $this->peticion('GET',"http://dsos.ito/api/ver_Api",['headers' => ['Content-Type' => 'application/x-www-form-urlenconded', 'X-Requested-With' => 'XMLHttpRequest'],'form_params' => 
   	['raza' => 'chihuahua']]);
   $retorno_datos = json_decode($respuesta);
   return response()->json($retorno_datos);
 }
 


 public function api_Registrar()
 {
  $respuesta = $this->peticion('POST',"http://dsos.ito/api/insertar_Api_Post",['headers' => ['Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'nombre' => 'yumi',
          'raza' => 'pitbull'
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
 }


 public function api_Actualizar()
 {
  $respuesta = $this->peticion('PUT',"http://dsos.ito/api/actualizar_Api_Put",['headers' => ['Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'id_var' => 5,
          'nombre_var' => 'chunchumaru',
          'raza_var' => 'san bernardo'
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
 }


 public function api_Eliminar()
 {
  $respuesta = $this->peticion('PUT',"http://dsos.ito/api/eliminar_Api_Get",['headers' => ['Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'id_var' => 1
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
 }


}