<?php
namespace App\Models\HolaMundo;
use Illuminate\Database\Eloquent\Model;

class Registro_M extends Model
{
    protected $table = 'agregard';
    protected $primarykey = 'id';
    //public $incrementing = true;
    public $timestamps = false;

    protected $fillable = ['id','nombre','apellidopaterno','apellidomaterno','usuario','pass','fecha'];
}
?>