<?php
namespace App\Http\Controllers\Unidad2;
use App\Http\Controllers\Controller;

use App\Models\Modelo2;
use App\Models\HolaMundo\Registro_M;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HolaMundoController extends Controller
{
    public function texto()
    {
        return view('HolaMundo/holamundo');
    }

    public function verConsulta()
    {
        $consulta = Modelo2::all();
        return $consulta;
    }
    public function insertar()
    {
        $insertar = Modelo2::create(['RFC' => 'SIJE971114SK1']);
        return 'Andre y Lalo son putos';
    }
    public function insertar2()
    {
        return view('Registro');
    }
    public function insertarf(Request $request)
    {
        $nombre = $request->input('nombre');
        $ap = $request->input('apellidop');
        $am = $request->input('apellidom');
        $us = $request->input('usuario');
        $con = bcrypt($request->input('contraseña'));
        $fecha = $request->input('fecha');

        $insertar=Registro_M::create(['nombre'=>$nombre,'apellidopaterno'=>$ap,'apellidomaterno'=>$am,
        'usuario'=>$us,'pass'=>$con,'fecha'=>$fecha,]);

        //return 'registrado correctamente';
        return redirect()->to('ver_todo');
        
    }

    public function eliminar_busqueda()
    {
        return view('Busqueda_Eli');
    }

    public function eliminarconbusqueda(Request $request)
    {
        $id = $request->input('busqueda');
        $eliminar = Registro_M::find($id);
        $eliminar ->delete(); 
        //return 'borrado con exito';
        return redirect()->to('ver_todo');
    }
    public function borrar()
    {
        $borrar = Modelo2::where('ID',2)->delete();
        return 'put o elquelo lea';
    }

    public function busqueda_actualizar()
    {
        return view('Busqueda_Actualizar');
    }

    public function VerDatosActualizarBusqueda(Request $request)
    {   $id = $request->input('busqueda');
        $consulta = Registro_M::where('id',$id)->take(1)->first();
        //return $consulta;
        return view('VerDatosActualizar')->with('modificar',$consulta);
    }

    public function actualizar_ok(Request $request)
    {
        $id = $request->input('id');
        $actualizar = Registro_M::find($id);
        $actualizar->Nombre=$request->nombres;
        $actualizar->apellidopaterno=$request->apellidop;
        $actualizar->apellidomaterno=$request->apellidom;
        $actualizar->usuario=$request->usuario;
        $actualizar->fecha=$request->fecha;
        $actualizar->pass=bcrypt($request->input('contraseña'));
        $actualizar -> save();
        //return 'actualizado correctamente';
        return redirect()->to('ver_todo');

    }

    public function VerDatosEliminar($id)
    {  // $id = $request->input('busqueda');
        $consulta = Registro_M::where('id',$id)->take(1)->first();
        //return 'si se ve';
        return view('ConfirmacionEliminar')->with('eliminar',$consulta);
    }

    

    public function editartodo($id)
    {
        $actualizar = Registro_M::find($id);
        return view('VerDatosActualizar')->with('modificar',$actualizar);

    }

    public function ver_tabla()
    {
          $tabla = Registro_M::all();

      return view('VerTodo')->with('x',$tabla);
    }

}
/*
En la carpera routes, web.php se pone la ruta a enmascarar 
    Route::get('pluto','Unidad2\HolaMundoController@texto');
 así la ruta original localhost:8000/holamundo pasaria a ser localhost:8000/pluto


*/