<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('tabla', 'Practica1\TablaController@verTabla');
Route::get('ver','Unidad2\HolaMundoController@texto');
Route::get('consulta','Unidad2\HolaMundoController@verConsulta');
Route::get('insertar','Unidad2\HolaMundoController@insertar');
Route::get('borrar','Unidad2\HolaMundoController@borrar');
Route::get('insertar2','Unidad2\HolaMundoController@insertar2');
Route::post('insertarf','Unidad2\HolaMundoController@insertarf');
Route::get('eliminar-busqueda','Unidad2\HolaMundoController@eliminar_busqueda');
Route::post('eliminado','Unidad2\HolaMundoController@eliminarconbusqueda');

Route::get('busquedaactualizar','Unidad2\HolaMundoController@busqueda_actualizar');
Route::post('VerDatos','Unidad2\HolaMundoController@VerDatosActualizarBusqueda');
Route::put('actualizarok','Unidad2\HolaMundoController@actualizar_ok');

Route::get('ver_todo','Unidad2\HolaMundoController@ver_tabla');
Route::get('editar/{id}','Unidad2\HolaMundoController@editartodo');
Route::get('eliminar/{id}','Unidad2\HolaMundoController@VerDatosEliminar');
Route::post('aliminar','Unidad2\HolaMundoController@eliminarconbusqueda');

//Practica 3

Route::get('altabienes','Practica3\Practica3Controller@altaBienes');
Route::post('insertarbienes','Practica3\Practica3Controller@insertarb');
Route::get('vertodobienes','Practica3\Practica3Controller@vertodobienes');

//Examen u2
Route::get('vinsertarusuario','ExamenU2\ExamenU2Controller@vinsertarusuario');
Route::post('insertarusuario','ExamenU2\ExamenU2Controller@insertarusuario');
Route::get('vertodousuario','ExamenU2\ExamenU2Controller@vertodousuario');
Route::get('vertodousuario2','ExamenU2\ExamenU2Controller@vertodousuario2');
Route::get('editar2/{id}','ExamenU2\ExamenU2Controller@editartodo');
Route::get('eliminar2/{id}','ExamenU2\ExamenU2Controller@VerDatosEliminar');
Route::post('aliminar2','ExamenU2\ExamenU2Controller@eliminarconbusqueda');
Route::put('actualizar','ExamenU2\ExamenU2Controller@actualizar_ok');
// juego------
Route::get('vinsertarjuego','ExamenU2\ExamenU2Controller@vinsertarjuego');
Route::post('insertarjuego','ExamenU2\ExamenU2Controller@insertarjuego');
Route::get('vertodojuego','ExamenU2\ExamenU2Controller@vertodojuego');
Route::get('vertodojuego2','ExamenU2\ExamenU2Controller@vertodojuego2');
Route::get('editar3/{id}','ExamenU2\ExamenU2Controller@editartodojuego');
Route::get('eliminar3/{id}','ExamenU2\ExamenU2Controller@VerDatosEliminarJuego');
Route::post('aliminar3','ExamenU2\ExamenU2Controller@eliminarconbusquedajuego');
Route::put('actualizar3','ExamenU2\ExamenU2Controller@actualizar_okjuego');
